VISIBLE_SIDE_BOTTOM_EXTENSION_RIGHT_FLAG -> "Bottom extension flag for visible right sides"
VISIBLE_SIDE_BACK_EXTENSION_RIGHT_FLAG -> "Back extension flag for visible right sides"

RIGHT_PANEL_FRONT_VARIANCE -> "Front clearance for right panel"
RIGHT_PANEL_BACK_VARIANCE -> "Back clearance for right panel"
RIGHT_PANEL_UPPER_VARIANCE -> "Upper clearance for right panel"
RIGHT_PANEL_LOWER_VARIANCE -> "Lower clearance for right panel"

CHANGE_SIDES_CODE -> "Change code of sides when they are visible"

PANEL_MAIN_ROTATION -> "Main rotation"
PANEL_CUTLIST_ROTATION -> "Grain rotation of cutlist"
PANEL_DXF_ROTATION -> "Dxf rotation of dxf output"
PANEL_MIRRORING_AXIS -> "Axis of mirroring operation"

PANEL_LABEL -> "Label of panel"
PANEL_TAG -> "Tag of panel"

VISIBLE_EDGESTRIP_MAT -> "Material of visible edgestrip"
HIDDEN_EDGESTRIP_MAT -> "Material of hidden edgestrip"
TOP_EDGESTRIP_MAT -> "Material of top edgestrip"
BOTTOM_EDGESTRIP_MAT -> "Material of bottom edgestrip"

RIGHT_PANEL_THICKNESS -> "Thickness of right panel"

; Corner unit right panel parameters
AK_RIGHT_PANEL_FRONT_VARIANCE -> "Front clearance for right panel"
AK_RIGHT_PANEL_BACK_VARIANCE -> "Back clearance for right panel"

AK_RIGHT_PANEL_OP_ON_LOWER_FACE -> "Operations are performed on lower surface of right panels"

AK_RIGHT_PANEL_MAIN_ROTATION -> "Main rotation"
AK_RIGHT_PANEL_CUTLIST_ROTATION -> "Grain rotation of cutlist"
AK_RIGHT_PANEL_DXF_ROTATION -> "Dxf rotation of dxf output"
AK_RIGHT_PANEL_MIRRORING_AXIS -> "Axis of mirroring operation"

AK_RIGHT_PANEL_LABEL -> "Label of panel"
AK_RIGHT_PANEL_TAG -> "Tag of panel"

AK_RIGHT_VISIBLE_EDGESTRIP_MAT -> "Material of visible edgestrip"
AK_RIGHT_HIDDEN_EDGESTRIP_MAT -> "Material of hidden edgestrip"
AK_RIGHT_TOP_EDGESTRIP_MAT -> "Material of top edgestrip"
AK_RIGHT_BOTTOM_EDGESTRIP_MAT -> "Material of bottom edgestrip"

AK_RIGHT_PANEL_THICKNESS -> "Thickness of corner unit right panel"

;Fridge cabinet right panel parameters
BC_RIGHT_SS_PANEL_MAIN_ROTATION -> "Main rotation for short side"
BC_RIGHT_SS_PANEL_CUTLIST_ROTATION -> "Grain rotation of cutlist for short side"
BC_RIGHT_SS_PANEL_DXF_ROTATION -> "Dxf rotation of dxf output for short side"
BC_RIGHT_SS_PANEL_MIRRORING_AXIS -> "Axis of mirroring operation for short side"

BC_RIGHT_SHORT_SIDE_LABEL -> "Label of short side"
BC_RIGHT_SHORT_SIDE_TAG -> "Tag of short side"

BC_RIGHT_SS_VISIBLE_EDGESTRIP_MAT -> "Visible edgestrip material for short side"
BC_RIGHT_SS_HIDDEN_EDGESTRIP_MAT -> "Hidden edgestrip material for short side"
BC_RIGHT_SS_TOP_EDGESTRIP_MAT -> "Top edgestrip material for short side"
BC_RIGHT_SS_BOTTOM_EDGESTRIP_MAT -> "Bottom edgestrip material for short side"

BC_RIGHT_LS_PANEL_MAIN_ROTATION -> "Main rotation for long side"
BC_RIGHT_LS_PANEL_CUTLIST_ROTATION -> "Grain rotation of cutlist for long side"
BC_RIGHT_LS_PANEL_DXF_ROTATION -> "Dxf rotation of dxf output for long side"
BC_RIGHT_LS_PANEL_MIRRORING_AXIS -> "Axis of mirroring operation for long side"

BC_RIGHT_LONG_SIDE_LABEL -> "Label of long side"
BC_RIGHT_LONG_SIDE_TAG -> "Tag of long side"

BC_RIGHT_LS_VISIBLE_EDGESTRIP_MAT -> "Visible edgestrip material for long side"
BC_RIGHT_LS_HIDDEN_EDGESTRIP_MAT -> "Hidden edgestrip material for long side"
BC_RIGHT_LS_TOP_EDGESTRIP_MAT -> "Top edgestrip material for long side"
BC_RIGHT_LS_BOTTOM_EDGESTRIP_MAT -> "Bottom edgestrip material for long side"

BC_RIGHT_PANEL_THICKNESS -> "Thickness of refrigerator unit right panel"

;prefvars
(if (null RIGHT_PANEL_DEFAULT_LOADED)
	(progn
		(_SETA 'VISIBLE_SIDE_BOTTOM_EXTENSION_RIGHT_FLAG T)
		(_SETA 'VISIBLE_SIDE_BACK_EXTENSION_RIGHT_FLAG T)

		(if (and (not (equal __RSIDEHEI 0)) (equal VISIBLE_SIDE_BOTTOM_EXTENSION_RIGHT_FLAG T))
			(set 'RIGHT_PANEL_LOWER_VARIANCE (- (convertLengthToSpecifiedUnit __RSIDEHEI (getvar "INSUNITS") 4) __HEI))
			(_SETA 'RIGHT_PANEL_LOWER_VARIANCE 0)
		)
		
		(if (and (not (equal __RSIDEWID 0)) (equal VISIBLE_SIDE_BACK_EXTENSION_RIGHT_FLAG T))
			(set 'RIGHT_PANEL_BACK_VARIANCE (- (convertLengthToSpecifiedUnit __RSIDEWID (getvar "INSUNITS") 4) __DEP))
			(_SETA 'RIGHT_PANEL_BACK_VARIANCE 0)
		)
				
		(_SETA 'RIGHT_PANEL_FRONT_VARIANCE 0)
		(_SETA 'RIGHT_PANEL_UPPER_VARIANCE 0)
		
		(_SETA 'CHANGE_SIDES_CODE nil)
		
		(_SETA 'PANEL_MAIN_ROTATION 0)
		(_SETA 'PANEL_CUTLIST_ROTATION nil)
		(_SETA 'PANEL_DXF_ROTATION nil)
		(_SETA 'PANEL_MIRRORING_AXIS nil)

		(_SETA 'PANEL_LABEL "LEFT/RIGHT_PANEL")
		(_SETA 'PANEL_TAG nil)

		(_SETA 'VISIBLE_EDGESTRIP_MAT nil)
		(_SETA 'HIDDEN_EDGESTRIP_MAT nil)
		(_SETA 'TOP_EDGESTRIP_MAT nil)
		(_SETA 'BOTTOM_EDGESTRIP_MAT nil)
		
		(_SETA 'RIGHT_PANEL_THICKNESS __AD_PANELTHICK)
		
		; If variances for upper and lower are different from 0, change joint types 
		(if (> RIGHT_PANEL_LOWER_VARIANCE 0)
			(progn
				(_FSET (_ 'PANELS_JUNCTION_STYLES_DEFAULT_LOADED nil))
				(_FSET (_ 'BOTTOM_PANEL_JOINT_TYPE 0))
				
				(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
			)
		)
		
		(if (> RIGHT_PANEL_UPPER_VARIANCE 0)
			(progn
				(_FSET (_ 'PANELS_JUNCTION_STYLES_DEFAULT_LOADED nil))
				(_FSET (_ 'TOP_PANEL_JOINT_TYPE 0))
				
				(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
			)
		)
		
		; Corner unit right panel parameters
		(_SETA 'AK_RIGHT_PANEL_FRONT_VARIANCE 0)
		(_SETA 'AK_RIGHT_PANEL_BACK_VARIANCE 0)

		(_SETA 'AK_RIGHT_PANEL_OP_ON_LOWER_FACE nil)

		(_SETA 'AK_RIGHT_PANEL_MAIN_ROTATION -90)
		(_SETA 'AK_RIGHT_PANEL_CUTLIST_ROTATION nil)
		(_SETA 'AK_RIGHT_PANEL_DXF_ROTATION nil)
		(_SETA 'AK_RIGHT_PANEL_MIRRORING_AXIS nil)

		(_SETA 'AK_RIGHT_PANEL_LABEL "LEFT/RIGHT_PANEL")
		(_SETA 'AK_RIGHT_PANEL_TAG nil)

		(_SETA 'AK_RIGHT_VISIBLE_EDGESTRIP_MAT nil)
		(_SETA 'AK_RIGHT_HIDDEN_EDGESTRIP_MAT nil)
		(_SETA 'AK_RIGHT_TOP_EDGESTRIP_MAT nil)
		(_SETA 'AK_RIGHT_BOTTOM_EDGESTRIP_MAT nil)
		
		(_SETA 'AK_RIGHT_PANEL_THICKNESS __AD_PANELTHICK)
		
		; CONTINGENCIES
		; AK_RIGHT_PANEL_OP_ON_LOWER_FACE
		(if (null (member AK_RIGHT_PANEL_OP_ON_LOWER_FACE (_ nil T))) (_FSET (_ 'AK_RIGHT_PANEL_OP_ON_LOWER_FACE T)))
	
		;Fridge cabinet right panel parameters
		(_SETA 'BC_RIGHT_SS_PANEL_MAIN_ROTATION 90)
		(_SETA 'BC_RIGHT_SS_PANEL_CUTLIST_ROTATION nil)
		(_SETA 'BC_RIGHT_SS_PANEL_DXF_ROTATION nil)
		(_SETA 'BC_RIGHT_SS_PANEL_MIRRORING_AXIS nil)
                
		(_SETA 'BC_RIGHT_SHORT_SIDE_LABEL "LEFT/RIGHT_PANEL")
		(_SETA 'BC_RIGHT_SHORT_SIDE_TAG nil)
                
		(_SETA 'BC_RIGHT_SS_VISIBLE_EDGESTRIP_MAT nil)
		(_SETA 'BC_RIGHT_SS_HIDDEN_EDGESTRIP_MAT nil)
		(_SETA 'BC_RIGHT_SS_TOP_EDGESTRIP_MAT nil)
		(_SETA 'BC_RIGHT_SS_BOTTOM_EDGESTRIP_MAT nil)
                
		(_SETA 'BC_RIGHT_LS_PANEL_MAIN_ROTATION -90)
		(_SETA 'BC_RIGHT_LS_PANEL_CUTLIST_ROTATION nil)
		(_SETA 'BC_RIGHT_LS_PANEL_DXF_ROTATION nil)
		(_SETA 'BC_RIGHT_LS_PANEL_MIRRORING_AXIS nil)
                
		(_SETA 'BC_RIGHT_LONG_SIDE_LABEL "LEFT/RIGHT_PANEL")
		(_SETA 'BC_RIGHT_LONG_SIDE_TAG nil)
                
		(_SETA 'BC_RIGHT_LS_VISIBLE_EDGESTRIP_MAT nil)
		(_SETA 'BC_RIGHT_LS_HIDDEN_EDGESTRIP_MAT nil)
		(_SETA 'BC_RIGHT_LS_TOP_EDGESTRIP_MAT nil)
		(_SETA 'BC_RIGHT_LS_BOTTOM_EDGESTRIP_MAT nil)
		
		(_SETA 'BC_RIGHT_PANEL_THICKNESS __AD_PANELTHICK)
		
		(_FSET (_ 'sidePanelFrontVarianceForTallGola (atof (iniread ad_MOD-INI "gola" "gola_tall_module_side_panel_front_variance"))))
		(if (and sidePanelFrontVarianceForTallGola (not (equal sidePanelFrontVarianceForTallGola 0.0))) 
			(progn
				(_FSET (_ 'tallGolaProfileLayerListStr (iniread ad_MOD-INI "gola" "gola_tall_module_profile_layer_list")))
				(_FSET (_ 'tallGolaProfileLayerList (splitstr tallGolaProfileLayerListStr ",")))
				
				(if (cadr (isSidesOccupiedByVerticalGolaProfile g_currentModulEnt tallGolaProfileLayerList))
					(_FSET (_ 'RIGHT_PANEL_FRONT_VARIANCE  (convertLengthToSpecifiedUnit sidePanelFrontVarianceForTallGola (getvar "INSUNITS") 4)))
				)
			)
		)
			
		(_FSET (_ 'RIGHT_PANEL_DEFAULT_LOADED T))
	)
)
; Standard Values
(_NONOTCH)
