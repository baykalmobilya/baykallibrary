; ---------------------------------------------------------------------------------------------------------
; Drawer Body
DRAWER_TYPE -> "Type of drawer. Types are ``NORMAL`` , ``HIDDEN`` , ``SINK``"
DRAWER_BODY_TYPE -> "Type of drawer body. Types are ``SLIDING`` , ``SYSTEM``"

USE_HIDDEN_DRAWER_DOOR -> "Use panel like door on hidden drawer"

USE_DOOR_AS_DRAWER_FRONT_PANEL -> "Use drawer door as drawer front panel"
DRAWER_CABIN_ELEV -> "Distance from bottom edge of drawer door to bottom of drawer"
CHANGE_DRAWER_WITH_BOUNDRIES -> "According to boundries, depth of drawer changes"
DRAWER_DEPTH_UPPERBOUND -> "Uppermost boundry for depth of drawer"

DRAWER_TOTAL_GAP_WID -> "Total drawer rail operating range"
DRAWER_TOTAL_GAP_DEP -> "Minimum clearance between module depth and drawer rail"
GAP_BETWEEN_DRAWER_AND_RAIL -> "Clearance between drawer depth and drawer rail"
DRAWER_BODY_HEI -> "Height of drawer body panels; back, front and sides"
DRAWER_PANELS_WID_DIFF -> "Difference between drawer bottom panel width and drawer back-front panel width"
DRAWER_PANELS_HEI_DIFF_TOP -> "Difference between drawer side panels height and drawer back-front panel height on topside"
DRAWER_PANELS_HEI_DIFF_BOTTOM -> "Difference between drawer side panels height and drawer back-front panel height on bottomside"

SINKU_WID_OF_DRAWER -> "Width of notched part on sink unit drawer bottom panel"
SINKU_DEP_OF_DRAWER -> "Height of notched part on sink unit drawer bottom panel"

HIDDEN_DRAWER_DOOR_HEI-> "Height of hidden drawer door"
HIDDEN_DRAWER_DOOR_WID-> "Width of hidden drawer door"
HIDDEN_DRAWER_DOOR_HEI_DIFF_BOTTOM-> "Difference between hidden drawer down of bottom panel and down of hidden drawer door"

DRAWER_BODY_MAT -> "Material of drawer body panels; back, front and sides"
DRAWER_BODY_THICKNESS -> "Thickness of drawer body panels; back, front and sides"
DRAWER_BOTTOM_PANEL_MAT -> "Material of drawer bottom panel"
DRAWER_BOTTOM_PANEL_THICKNESS -> "Thickness of drawer bottom panel"
DRAWER_BOTTOM_PANEL_EXTRA_WIDTH_VARIANCE -> "Extra width variance for drawer bottom panel"
DRAWER_BOTTOM_PANEL_EXTRA_DEPTH_VARIANCE -> "Extra depth variance for drawer bottom panel"

; Hidden drawer extra panel
IS_THERE_EXTRA_PANEL_FOR_HIDDEN_DRAWER -> "Is there extra part between hidden drawer and unit body?"
HIDDEN_DRAWER_EXTRA_PANEL_HEI -> "Hidden drawer extra part height"
HIDDEN_DRAWER_EXTRA_PANEL_WID -> "Hidden drawer extra part width"
HIDDEN_DRAWER_EXTRA_PANEL_THICKNESS -> "Hidden drawer extra part thickness"

; Drawer Body Assembly parameters
GROOVE_MODE_FOR_BODY_ASSEMBLY -> "Mode of drawer body assembly with groove. Modes are ``WID``,``HEI``,``BOTH``"
PERFORM_GROOVE_ON_PANELS -> "Perform groove operation on drawer panels"
RAIL_DOOR_GROOVE_BOTTOM_OFFSET_VALUE -> "Groove offset distance to bottom of door"

DBA_GROOVE_DISTANCE -> "Groove distance of drawer body assembly"
DBA_GROOVE_WID -> "Groove width of drawer body assembly"
DBA_GROOVE_DEPTH -> "Groove depth of drawer body assembly"

IS_THINNING_OP_AVAILABLE_FOR_BOTTOM -> "Is thinning operation available for bottom panel?"
IS_THINNING_OP_AVAILABLE_FOR_BACK -> "Is thinning operation available for back panel?"
DRAWER_PANELS_THINNING_OP_TYPE -> "Operation type of drawer panels thinning. Modes are ``POCKETING`` , ``MILLING``"
THINNING_OVERFLOW_VALUE -> "Value of begining of thinning operation from outside"
THINNING_WIDTH_BOTTOM -> "Width of thinning for left and right edges of drawer bottom panel"
THINNING_DEPTH_BOTTOM -> "Depth of thinning for left and right edges of drawer bottom panel"
THINNING_WIDTH_BACK -> "Width of thinning for left and right edges of drawer back panel"
THINNING_DEPTH_BACK -> "Depth of thinning for left and right edges of drawer back panel"

RAIL_BORDURE_CODE -> "Rail bordure code"
IS_RAIL_BORDURE_AVAILABLE -> "Is there bordure in rail package"
RAIL_BACK_PANEL_HOLDER_CODE -> "Rail back panel holder code"
IS_RAIL_BACK_PANEL_HOLDER_AVAILABLE -> "Is there back panel holder in rail package?"
; ---------------------------------------------------------------------------------------------------------
; Drawer Door Holes
DRAWER_DOOR_HOLES_AVAILABLE -> "Are drawer door holes available?"

DRAWER_DOOR_HOLES_OFFSET -> "Offset from drawer door holes to vertical edges of drawer door"
DRAWER_DOOR_FIRST_HOLE_DISTANCE -> "Distance from first drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_SECOND_HOLE_DISTANCE -> "Distance from second drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_THIRD_HOLE_DISTANCE -> "Distance from third drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_HOLES_DIAMETER -> "Diameter of drawer door holes"
DRAWER_DOOR_HOLES_DEPTH -> "Depth of drawer door holes"
DRAWER_DOOR_EXTRA_HOLES_NUMBER -> "Number of drawer door extra holes"
DRAWER_DOOR_FIRST_EXTRA_HOLE_DISTANCE -> "Distance from first extra drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_SECOND_EXTRA_HOLE_DISTANCE -> "Distance from second extra drawer door hole to bottom edge of drawer door"
DRAWER_DOOR_EXTRA_HOLES_DIAMETER -> "Diameter of drawer door extra holes"
DRAWER_DOOR_EXTRA_HOLES_DEPTH -> "Depth of drawer door extra holes"
DRAWER_DOOR_HEI_REQUIRED_FOR_THIRD_HOLE -> "Required door height to perform third drawer door hole"

IS_HDRAWER_DOOR_CONN_HOLE_AVAILABLE -> "Is drawer door-hidden drawer connection mechanism hole available"
HDRAWER_DOOR_CONN_HOLE_DIAMETER -> "Diameter of drawer door-hidden drawer connection mechanism hole"
HDRAWER_DOOR_CONN_HOLE_DEPTH -> "Depth of drawer door-hidden drawer connection mechanism hole"
HDRAWER_DOOR_CONN_HOLE_DISTANCE -> "Distance from topside of drawer door to drawer door-hidden drawer connection mechanism hole"
HDRAWER_DOOR_CONN_MECHANISM_CODE -> "Code of drawer door-hidden drawer connection mechanism"

IS_DRAWER_BACK_PANEL_HOLE_FOR_RAIL_AVAILABLE -> "Is drawer back panel hole for rail available"
DRAWER_BACK_PANEL_HOLE_DIA_FOR_RAIL -> "Drawer back panel hole diameter for rail"
DRAWER_BACK_PANEL_HOLE_DEP_FOR_RAIL -> "Drawer back panel hole depth for rail"
DRAWER_BACK_PANEL_HOLE_DOWN_OFFSET_FOR_RAIL -> "Drawer back panel hole offset from down edge of back panel"
DRAWER_BACK_PANEL_HOLE_SIDE_OFFSET_FOR_RAIL -> "Drawer back panel hole offset from side edges of back panel"

; ---------------------------------------------------------------------------------------------------------
; Hidden drawer extra panel
IS_THERE_EXTRA_PANEL_FOR_HIDDEN_DRAWER -> "Is there extra part between hidden drawer and unit body?"
HIDDEN_DRAWER_EXTRA_PANEL_HEI -> "Hidden drawer extra part height"
HIDDEN_DRAWER_EXTRA_PANEL_WID -> "Hidden drawer extra part width"
HIDDEN_DRAWER_EXTRA_PANEL_THICKNESS -> "Hidden drawer extra part thickness"

; Hidden drawer extra panel holes
ARE_EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_AVAILABLE -> "Are extra part holes for hidden drawer and unit body available?"
EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_LIST -> "Hidden drawer extra part holes list"
EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_BOTTOM_DISTANCE -> "Hidden drawer extra part holes distance to part's bottom side"
EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_DEPTH -> "Hidden drawer extra part holes depth"
EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_DIA -> "Hidden drawer extra part holes diameter"
EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_FOR_SIDES_SHIFTING_VALUE -> "Hidden drawer extra part holes for sides shifting value"
EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_HOLES_HEI_DIFF_WITH_RAIL_HOLES_FOR_SIDES -> "Hidden drawer extra part holes height difference between extra panel and rail holes"
EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_DEPTH_FOR_SIDES -> "Hidden drawer extra part holes depth for sides"
EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_DIA_FOR_SIDES -> "Hidden drawer extra part holes diameter for sides"

; ---------------------------------------------------------------------------------------------------------
; Drawer Rail Holes
RAIL_CODE -> "Code of drawer rail"
RAIL_HOLES_AVAILABLE -> "Are rail holes available"

RAIL_HOLES_DIAMETER -> "Diameter of drawer rail holes"
RAIL_HOLES_DEPTH -> "Depth of drawer rail holes"

RAIL_LENGTHS -> "Rail lengths"
RAIL_HOLES_Y_AXIS -> "Rail holes y-axis offset from bottom of drawer body"
RAIL_HOLES_X_AXIS_LIST -> "Rail holes x-axis offsets from door of drawer"
RAIL_HOLES_NUMBER -> "Active rail hole number"
RAIL_HOLES_RAIL_LENGTH_LIMITS -> "Rail holes length limits"

;prefvars
; (if (null DRAWER_DEFAULT_LOADED)
	; (progn
		; ---------------------------------------------------------------------------------------------------------
		(_SETA 'DRAWER_TYPE "NORMAL")
		
		(_SETA 'DRAWER_BODY_TYPE "SYSTEM")
		; Drawer-module related parameters
		(_SETA 'USE_HIDDEN_DRAWER_DOOR T)
		(_SETA 'USE_DOOR_AS_DRAWER_FRONT_PANEL T)
		(_SETA 'DRAWER_CABIN_ELEV 14.5)
		(_SETA 'CHANGE_DRAWER_WITH_BOUNDRIES T)
		(_SETA 'DRAWER_DEPTH_UPPERBOUND 500)
		; Drawer body parameters
		(_SETA 'DRAWER_TOTAL_GAP_WID 21)
		(_SETA 'DRAWER_TOTAL_GAP_DEP 1)
		(_SETA 'GAP_BETWEEN_DRAWER_AND_RAIL 10)
		(_SETA 'DRAWER_BODY_HEI 155)
		(_SETA 'DRAWER_PANELS_WID_DIFF 21)
		(_SETA 'DRAWER_PANELS_HEI_DIFF_TOP 0)
		(_SETA 'DRAWER_PANELS_HEI_DIFF_BOTTOM 0)
		; Sink unit drawer parameters
		(_SETA 'SINKU_WID_OF_DRAWER 598)
		(_SETA 'SINKU_DEP_OF_DRAWER 284)
		
		;hidden drawer panel like door parameters
		(_SETA 'HIDDEN_DRAWER_DOOR_HEI 250)
		(_SETA 'HIDDEN_DRAWER_DOOR_WID (- __WID __AD_PANELTHICK __AD_PANELTHICK 6.0 ))
		(_SETA 'HIDDEN_DRAWER_DOOR_HEI_DIFF_BOTTOM 20)
		
		; Material and thickness parameters of drawer panels
		(_SETA 'DRAWER_BODY_MAT "D143_ANTRASIT_16")
		(_SETA 'DRAWER_BODY_THICKNESS 16)
		(_SETA 'DRAWER_BOTTOM_PANEL_MAT "D143_ANTRASIT_16")
		(_SETA 'DRAWER_BOTTOM_PANEL_THICKNESS 16)
		(_SETA 'DRAWER_BOTTOM_PANEL_EXTRA_WIDTH_VARIANCE 0)
		(_SETA 'DRAWER_BOTTOM_PANEL_EXTRA_DEPTH_VARIANCE 0)
		(_SETA 'GROOVE_MODE_FOR_BODY_ASSEMBLY "BOTH")

		(_SETA 'PERFORM_GROOVE_ON_PANELS nil)
		(_SETA 'RAIL_DOOR_GROOVE_BOTTOM_OFFSET_VALUE 0)

		(_SETA 'DBA_GROOVE_DISTANCE 12)
		(_SETA 'DBA_GROOVE_WID 8)
		(_SETA 'DBA_GROOVE_DEPTH 8)

		(_SETA 'IS_THINNING_OP_AVAILABLE_FOR_BOTTOM nil)
		(_SETA 'IS_THINNING_OP_AVAILABLE_FOR_BACK nil)
		(_SETA 'DRAWER_PANELS_THINNING_OP_TYPE "POCKETING")
		(_SETA 'THINNING_OVERFLOW_VALUE 0)
		(_SETA 'THINNING_WIDTH_BOTTOM 38)
		(_SETA 'THINNING_DEPTH_BOTTOM 8)
		(_SETA 'THINNING_WIDTH_BACK 38)
		(_SETA 'THINNING_DEPTH_BACK 8)

		(_SETA 'RAIL_BORDURE_CODE "BORDURE")
		(_SETA 'IS_RAIL_BORDURE_AVAILABLE nil)
		(_SETA 'RAIL_BACK_PANEL_HOLDER_CODE "HOLDER")
		(_SETA 'IS_RAIL_BACK_PANEL_HOLDER_AVAILABLE nil)
		; ---------------------------------------------------------------------------------------------------------		
		; Drawer door holes parameters
		(_SETA 'DRAWER_DOOR_HOLES_AVAILABLE T)
		
		(_SETA 'DRAWER_DOOR_HOLES_OFFSET (+ (- (* g_ClearBaseXSide 10)) __AD_PANELTHICK 14.5))
		(_SETA 'DRAWER_DOOR_FIRST_HOLE_DISTANCE (+ DRAWER_CABIN_ELEV 52))
		(_SETA 'DRAWER_DOOR_SECOND_HOLE_DISTANCE (+ DRAWER_DOOR_FIRST_HOLE_DISTANCE 32))
		(_SETA 'DRAWER_DOOR_THIRD_HOLE_DISTANCE (+ DRAWER_DOOR_SECOND_HOLE_DISTANCE 96))
		(_SETA 'DRAWER_DOOR_HOLES_DIAMETER 8)
		(_SETA 'DRAWER_DOOR_HOLES_DEPTH 14)
		(_SETA 'DRAWER_DOOR_EXTRA_HOLES_NUMBER 0)
		(_SETA 'DRAWER_DOOR_FIRST_EXTRA_HOLE_DISTANCE (+ DRAWER_DOOR_THIRD_HOLE_DISTANCE 32))
		(_SETA 'DRAWER_DOOR_SECOND_EXTRA_HOLE_DISTANCE (+ DRAWER_DOOR_FIRST_EXTRA_HOLE_DISTANCE 32))
		(_SETA 'DRAWER_DOOR_EXTRA_HOLES_DIAMETER 5)
		(_SETA 'DRAWER_DOOR_EXTRA_HOLES_DEPTH 14.0)
		(_SETA 'DRAWER_DOOR_HEI_REQUIRED_FOR_THIRD_HOLE 270)
		
		(_SETA 'IS_DRAWER_DOOR_ADITIONAL_HOLE_AVAILABLE T)
		(_SETA 'DRAWER_DOOR_ADITIONAL_HOLES_LIST (list (list 9 20) (list 9 313)))
		(_SETA 'DRAWER_DOOR_ADITIONAL_HOLES_DIAMETER 8)
		(_SETA 'DRAWER_DOOR_ADITIONAL_HOLES_DEPTH 13)
		
		
		(_SETA 'IS_HDRAWER_DOOR_CONN_HOLE_AVAILABLE nil)
		(_SETA 'HDRAWER_DOOR_CONN_HOLE_DIAMETER 25)
		(_SETA 'HDRAWER_DOOR_CONN_HOLE_DEPTH 10)
		(_SETA 'HDRAWER_DOOR_CONN_HOLE_DISTANCE 17)
		(_SETA 'HDRAWER_DOOR_CONN_MECHANISM_CODE "DRAWER_DOOR_HIDDEN_DRAWER_CONNECTION_MECHANISM")
		
		(_SETA 'IS_DRAWER_BACK_PANEL_HOLE_FOR_RAIL_AVAILABLE nil)
		(_SETA 'DRAWER_BACK_PANEL_HOLE_DIA_FOR_RAIL 8.0)
		(_SETA 'DRAWER_BACK_PANEL_HOLE_DEP_FOR_RAIL 14.0)
		(_SETA 'DRAWER_BACK_PANEL_HOLE_DOWN_OFFSET_FOR_RAIL 4.5)
		(_SETA 'DRAWER_BACK_PANEL_HOLE_SIDE_OFFSET_FOR_RAIL 7.25)
				
		; ---------------------------------------------------------------------------------------------------------
		;hidden drawer extra part
		(_SETA 'IS_THERE_EXTRA_PANEL_FOR_HIDDEN_DRAWER nil)
		(_SETA 'HIDDEN_DRAWER_EXTRA_PANEL_HEI 60)
		(_SETA 'HIDDEN_DRAWER_EXTRA_PANEL_WID 490)
		(_SETA 'HIDDEN_DRAWER_EXTRA_PANEL_THICKNESS 25)

		; Hidden drawer extra panel holes
		(_SETA 'ARE_EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_AVAILABLE nil)
		(_SETA 'EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_LIST (_ 60 245 430))
		(_SETA 'EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_BOTTOM_DISTANCE 30)
		(_SETA 'EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_DEPTH 14)
		(_SETA 'EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_DIA 5)
		(_SETA 'EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_FOR_SIDES_SHIFTING_VALUE 36)
		(_SETA 'EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_HOLES_HEI_DIFF_WITH_RAIL_HOLES_FOR_SIDES 10)
		(_SETA 'EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_DEPTH_FOR_SIDES 14)
		(_SETA 'EXTRA_PANEL_HOLES_FOR_HIDDEN_DRAWER_DIA_FOR_SIDES 5)

		; ---------------------------------------------------------------------------------------------------------
		; Drawer rail holes parameters
		(_SETA 'RAIL_CODE "GRASS_VIO")
		(_SETA 'RAIL_HOLES_AVAILABLE T)

		(_SETA 'RAIL_HOLES_DIAMETER 5.0)
		(_SETA 'RAIL_HOLES_DEPTH 14)
		
		; Rail params
		(_SETA 'RAIL_LENGTHS (_ 270 300 350 400 450 500 550 600 650))
		
		(_SETA 'RAIL_HOLES_Y_AXIS 42)
		(_SETA 'RAIL_HOLES_X_AXIS_LIST (_ 19 37 147 165 243 261 307 325))
		(_SETA 'RAIL_HOLES_NUMBER 8)
		
		;First rail hole is default and will be available all the time
		(_SETA 'RAIL_HOLES_RAIL_LENGTH_LIMITS (_ 0 270 270 350 350))

		;Creating all available holes list
		(_FSET (_ 'RAIL_HOLES_LIST nil))
		(foreach xAxisValue RAIL_HOLES_X_AXIS_LIST
			(_FSET (_ 'RAIL_HOLES_LIST (cons (_ RAIL_HOLES_DIAMETER RAIL_HOLES_DEPTH (_ xAxisValue RAIL_HOLES_Y_AXIS 0)) RAIL_HOLES_LIST)))
		)
		(_FSET (_ 'RAIL_HOLES_LIST (reverse RAIL_HOLES_LIST)))
			
		;Creating rail holes list for rail packages
		(_FSET (_ 'RAIL_HOLES_FOR_RAIL_PACKAGES nil))
		(foreach railLength RAIL_LENGTHS
			(_FSET (_ 'RAIL_LENGTH_RELATED_HOLE_LIST nil))
			(setq i 0)
			(while (< i RAIL_HOLES_NUMBER)
				(if (>= railLength (nth i RAIL_HOLES_RAIL_LENGTH_LIMITS))
					(progn
						(_FSET (_ 'RAIL_LENGTH_RELATED_HOLE_LIST (cons (nth i RAIL_HOLES_LIST) RAIL_LENGTH_RELATED_HOLE_LIST)))
					)
				)
				(i++)
			)
			(_FSET (_ 'RAIL_LENGTH_RELATED_HOLE_LIST (reverse RAIL_LENGTH_RELATED_HOLE_LIST)))
			(_FSET (_ 'RAIL_HOLES_FOR_RAIL_PACKAGES (cons (append (_ railLength (_& (_ RAIL_CODE "_" (convertStr railLength)))) RAIL_LENGTH_RELATED_HOLE_LIST) RAIL_HOLES_FOR_RAIL_PACKAGES)))
		)
		(_FSET (_ 'RAIL_HOLES_FOR_RAIL_PACKAGES (reverse RAIL_HOLES_FOR_RAIL_PACKAGES)))

		; ---------------------------------------------------------------------------------------------------------
		; CONTINGENCIES
		; USE_DOOR_AS_DRAWER_FRONT_PANEL
		(if (null (member USE_DOOR_AS_DRAWER_FRONT_PANEL (_ nil T))) (_FSET (_ 'USE_DOOR_AS_DRAWER_FRONT_PANEL T)))
		; CHANGE_DRAWER_WITH_BOUNDRIES
		(if (null (member CHANGE_DRAWER_WITH_BOUNDRIES (_ nil T))) (_FSET (_ 'CHANGE_DRAWER_WITH_BOUNDRIES nil)))
		; HIDDEN DRAWER CONNECTOR MECHANISM UNIT
		(if (null HDRAWER_DOOR_CONN_MECHANISM_UNIT) (_FSET (_ 'HDRAWER_DOOR_CONN_MECHANISM_UNIT "pc")))
		
		; (_FSET (_ 'DRAWER_DEFAULT_LOADED T))
	; )
; )

; PRELIMINARY CONTROLS
; (if (null DRAWER_BODY_PRELIMINARY_CONTROLS)
	; (progn
		(if (null CHANGE_DRAWER_WITH_BOUNDRIES)
			(_FSET (_ 'DRAWER_DEPTH (- __DEP DRAWER_TOTAL_GAP_DEP)))	; Height of panel directly relates to module depth, not boundries
			(_FSET (_ 'DRAWER_DEPTH DRAWER_DEPTH_UPPERBOUND)) ; Boundry for actual module calculated here. Via this boundry, drawer bottom panels height will be calculated
		)
		
		; According to the notch type, possible maximum drawer depth and actualDrawerDepth are calculated
		(if (or (equal __NOTCHTYPE 0) (equal __NOTCHTYPE 4))
			(progn
				(_FSET (_ 'maxDrawerDepthPossible __DEP))
				(_FSET (_ 'actualDrawerDepth DRAWER_DEPTH))
			)
			(progn
				(_FSET (_ 'maxDrawerDepthPossible (- __DEP __NOTCHDIM2)))
				(_FSET (_ 'actualDrawerDepth (- DRAWER_DEPTH __NOTCHDIM2)))
			)
		)
		
		(_FSET (_ 'paramBody (_& (_ "CDOOR_" __CURDIVORDER "_1_"))))
		(_FSET (_ 'currentDoorELEV (_S2V (_& (_ paramBody "ELEV")))))
		; General validation control for RAIL_HOLES_FOR_RAIL_PACKAGES
		(if (and (_NOTNULL RAIL_HOLES_FOR_RAIL_PACKAGES) (equal (type RAIL_HOLES_FOR_RAIL_PACKAGES) 'LIST))
			(progn
				; LIMIT CONTROLS
				(_FSET (_ 'loopControl nil))
				(_FSET (_ 'numberOfPackages (length RAIL_HOLES_FOR_RAIL_PACKAGES)))
				(_FSET (_ 'packageCounter 1))
				(_FSET (_ 'limitOfRail 0))
				(_FSET (_ 'indexOfRail nil))
				(while (and (null loopControl) (< packageCounter (+ numberOfPackages 1)))
					(_FSET (_ 'packageIndex (- packageCounter 1)))
					(_FSET (_ 'currentPackage (getnth packageIndex RAIL_HOLES_FOR_RAIL_PACKAGES)))
					(_FSET (_ 'currentPackLimit (getnth 0 currentPackage)))
					(cond
						((equal actualDrawerDepth currentPackLimit)
							; No need for further investigation. We are sure this is the package we need
							(_FSET (_ 'loopControl T))
							(_FSET (_ 'limitOfRail currentPackLimit))
							(_FSET (_ 'indexOfRail packageIndex))
						)
						((> actualDrawerDepth currentPackLimit)
							; It is a valid package but is it the best choice for us???
							(if (> currentPackLimit limitOfRail)
								(progn
									(_FSET (_ 'limitOfRail currentPackLimit))
									(_FSET (_ 'indexOfRail packageIndex))
								)
							)
						)
					)
					(_FSET (_ 'packageCounter (+ 1 packageCounter)))
				)
				
				(if (_NOTNULL indexOfRail)
					(_FSET (_ 'activeRailPackage (getnth indexOfRail RAIL_HOLES_FOR_RAIL_PACKAGES)))
				)
			)
		)
		
		; (_FSET (_ 'DRAWER_BODY_PRELIMINARY_CONTROLS T))
	; )
; )
(_NONOTCH)

; IMPORTANT INFORMATION
; Format of RAIL_HOLES_FOR_RAIL_PACKAGES

; Format of RAIL_HOLES_FOR_RAIL_PACKAGES list 	-> 		list RAIL_PACKAGE_1 RAIL_PACKAGE_2 ....
; Format of RAIL_PACKAGE_1 list 			  	->		list upperLimitForRail RAIL_PACKAGE_CODE firstHoleParams secondHoleParams ....
; Format of firstHoleParams list			  	->		list holeDiameter holeDepth holePosition
; Format of holePosition list				  	->		list positionOnAxisX positionOnAxisY positionOnAxisZ
