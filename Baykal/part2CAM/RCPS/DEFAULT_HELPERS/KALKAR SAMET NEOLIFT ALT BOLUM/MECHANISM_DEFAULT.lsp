; General mechanism parameters
NO_NEED_MECHANISM_HOLES_ON_DOORS -> "There is no need to perform mechanism holes on doors"

MECHANISM_SIDE -> "Which side mechanism connects"
ARE_MECHANISM_HOLES_DISTANCES_FROM_MODULE_TOP -> "Are side holes' distances of mechanism from top of module?"
ARE_MECHANISM_HOLES_DISTANCES_FROM_DIV_TOP -> "Are side holes' distances of mechanism from top of division?"
LIMIT_FOR_TWO_MECHANISMS -> "Door width limit to perform mechanism holes on both sides"
MECHANISM_HOLES_LIST_ON_SIDES -> "List of mechanism holes on sides. Each element of holes list is a list and consists of horizontal offset, vertical offset. For example (_(_ 18.0 131.0) (_ 18.0 120.0) ...)"
MECHANISM_HOLES_DIA -> "Mechanism hole diameter"
MECHANISM_HOLES_DEPTH -> "Mechanism hole depth"
MECHANISM_HOLES_UPPER_VER_VARIANCE -> "Variance for vertical offset of holes on upper division"
MECHANISM_HOLES_UPPER_HOR_VARIANCE -> "Variance for horizontal offset of holes on upper division"

MECHANISM_DOOR_HOLES_LIST -> "List of mechanism holes on door. Each element of holes list is a list and consists of horizontal offset, vertical offset. For example (_ (_ 30.0 70.0) (_ 30.0 95.0) ...)"
MECHANISM_DOOR_HOLES_DIA -> "Diameter of mechanism holes on doors"
MECHANISM_DOOR_HOLES_DEPTH -> "Depth of mechanism holes on doors"

; Parameters for bifolding door module mechanisms
IS_MECHANISM_FOR_BIFOLDING_DOOR_MODULE -> "Is mechanism for bifolding door module?"
MECHANISM_TOP_DOOR_DIV_NUMBER -> "Top door division number for drilling hinge holes between two doors for mechanism"

ARE_OPERATIONS_BETWEEN_DOORS_AVAILABLE -> "Are operations between two doors for bifolding door modules available?"
MECHANISM_TOP_DOOR_HOLES_LIST -> "Top door holes' list for bifolding door operations"
MECHANISM_BOTTOM_DOOR_HOLES_LIST -> "Bottom door holes' list for bifolding door operations"
MECHANISM_DOOR_HOLES_DIA_FOR_BIFOLD_DOORS -> "Hole diameter for bifolding door mechanism"
MECHANISM_DOOR_HOLES_DEPTH_FOR_BIFOLD_DOORS -> "Hole depth for bifolding door mechanism"

; Power unit parameters
IS_POWER_UNIT_AVAILABLE_FOR_MECHANISM -> "Is there power unit for mechanism?"
POWER_UNIT_LIMITS_LIST -> "Power unit limit parameters' list"
POWER_UNIT_CODES_LIST -> "Power unit code list"

POWER_UNIT_QUANTITY -> "Power unit quantity"
UNIT_OF_POWER_UNIT -> "Unit of power unit"

MECHANISM_CODE -> "Code of mechanism"
MECHANISM_UNIT -> "Unit of mechanism"
MECHANISM_QUANTITY -> "Quantity of mechanism"
;prefvars
; (if (null MECHANISM_DEFAULT_LOADED)
	; (progn
		; General mechanism parameters
		(_SETA 'NO_NEED_MECHANISM_HOLES_ON_DOORS nil)
		
		(_SETA 'MECHANISM_SIDE "L")
		(_SETA 'ARE_MECHANISM_HOLES_DISTANCES_FROM_MODULE_TOP nil)
		(_SETA 'ARE_MECHANISM_HOLES_DISTANCES_FROM_DIV_TOP nil)
		(_SETA 'LIMIT_FOR_TWO_MECHANISMS 600)
		(_SETA 'MECHANISM_HOLES_LIST_ON_SIDES (_ (_ 42.0 60.0) (_ 42.0 92.0)))
		(_SETA 'MECHANISM_HOLES_DIA 5.0)
		(_SETA 'MECHANISM_HOLES_DEPTH 5.0)
		(_SETA 'MECHANISM_HOLES_UPPER_VER_VARIANCE 0)
		(_SETA 'MECHANISM_HOLES_UPPER_HOR_VARIANCE 0)
		
		(_SETA 'MECHANISM_DOOR_HOLES_LIST (_ (_ 12.5 70.0) (_ 12.5 98.0)))
		(_SETA 'MECHANISM_DOOR_HOLES_DIA 5)
		(_SETA 'MECHANISM_DOOR_HOLES_DEPTH 5)
		
		; Parameters for bifolding door module mechanisms
		(_SETA 'IS_MECHANISM_FOR_BIFOLDING_DOOR_MODULE nil)
		(_SETA 'MECHANISM_TOP_DOOR_DIV_NUMBER 1)
		
		(_SETA 'ARE_OPERATIONS_BETWEEN_DOORS_AVAILABLE nil)
		(_SETA 'MECHANISM_TOP_DOOR_HOLES_LIST (_ (_ 10.0 10.0) (_ 10.0 20.0)))
		(_SETA 'MECHANISM_BOTTOM_DOOR_HOLES_LIST (_ (_ 10.0 10.0) (_ 10.0 20.0)))
		(_SETA 'MECHANISM_DOOR_HOLES_DIA_FOR_BIFOLD_DOORS 5)
		(_SETA 'MECHANISM_DOOR_HOLES_DEPTH_FOR_BIFOLD_DOORS 5)
		
		(_SETA 'MECHANISM_CODE "SAMET_NEOLIFT")
		(_SETA 'MECHANISM_QUANTITY 1)
		(_SETA 'MECHANISM_UNIT "pc")
		
		; Power unit parameters
		(_SETA 'IS_POWER_UNIT_AVAILABLE_FOR_MECHANISM nil)
		(_SETA 'POWER_UNIT_LIMITS_LIST (_ 400.0 500.0 550.0))
		(_SETA 'POWER_UNIT_CODES_LIST (_ "POWER_UNIT_1" "POWER_UNIT_2" "POWER_UNIT_3" "POWER_UNIT_4"))
		
		(_SETA 'POWER_UNIT_QUANTITY 1)
		(_SETA 'UNIT_OF_POWER_UNIT "pc")
		
		(if (null MECHANISM_UNIT) (_FSET (_ 'MECHANISM_UNIT "pc")))
		(if (null MECHANISM_QUANTITY) (_FSET (_ 'MECHANISM_QUANTITY 1)))
		
		; CONTINGENCIES
		; NO_NEED_MECHANISM_HOLES_ON_DOORS
		(if (null (member NO_NEED_MECHANISM_HOLES_ON_DOORS (_ nil T))) (_FSET (_ 'NO_NEED_MECHANISM_HOLES_ON_DOORS T)))
		; ARE_MECHANISM_HOLES_DISTANCES_FROM_MODULE_TOP
		(if (null (member ARE_MECHANISM_HOLES_DISTANCES_FROM_MODULE_TOP (_ nil T))) (_FSET (_ 'ARE_MECHANISM_HOLES_DISTANCES_FROM_MODULE_TOP nil)))
		; ARE_MECHANISM_HOLES_DISTANCES_FROM_DIV_TOP
		(if (null (member ARE_MECHANISM_HOLES_DISTANCES_FROM_DIV_TOP (_ nil T))) (_FSET (_ 'ARE_MECHANISM_HOLES_DISTANCES_FROM_DIV_TOP nil)))
		; IS_MECHANISM_FOR_BIFOLDING_DOOR_MODULE
		(if (null (member IS_MECHANISM_FOR_BIFOLDING_DOOR_MODULE (_ nil T))) (_FSET (_ 'IS_MECHANISM_FOR_BIFOLDING_DOOR_MODULE nil)))
		; ARE_OPERATIONS_BETWEEN_DOORS_AVAILABLE
		(if (null (member ARE_OPERATIONS_BETWEEN_DOORS_AVAILABLE (_ nil T))) (_FSET (_ 'ARE_OPERATIONS_BETWEEN_DOORS_AVAILABLE nil)))
		; IS_POWER_UNIT_AVAILABLE_FOR_MECHANISM
		(if (null (member IS_POWER_UNIT_AVAILABLE_FOR_MECHANISM (_ nil T))) (_FSET (_ 'IS_POWER_UNIT_AVAILABLE_FOR_MECHANISM nil)))
		; MECHANISM_SIDE
		(if (null (member MECHANISM_SIDE (_ "L" "R" "LR" "RL"))) (_FSET (_ 'MECHANISM_SIDE "L")))
		
		; (_FSET (_ 'MECHANISM_DEFAULT_LOADED T))
	; )
; )
(_NONOTCH)
