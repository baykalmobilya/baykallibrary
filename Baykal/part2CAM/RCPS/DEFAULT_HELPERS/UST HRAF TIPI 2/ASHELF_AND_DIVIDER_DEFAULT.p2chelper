ASHELF_FRONT_OFFSET -> "Front clearance for adjustable shelves"
ASHELF_BACK_OFFSET  -> "Back clearance for adjustable shelves"
ASHELF_SIDES_TOTAL_OFFSET -> "Total clearance of adjustable shelves from both sides"
GLASS_SHELF_THICKNESS -> "Thickness of adjustable glass shelf"

ASHELF_HOLE_DIAMETER -> "Diameter of shelf pin holes"
ASHELF_HOLE_DEPTH -> "Depth of shelf pin holes"
SHELF_HOLES_EXTRA_UP -> "Number of holes higher than reference hole"
SHELF_HOLES_EXTRA_DOWN -> "Number of holes lower than reference hole"
HOLE_GROUP_DISTANCE -> "Distance between shelf pin holes"
ASHELF_BACK_HOLE_OFFSET -> "Offset from back hole to backside of adjustable shelf"
ASHELF_FRONT_HOLE_OFFSET -> "Offset from front hole to frontside of adjustable shelf"

ASHELF_OPERATION -> "Adjustable shelf operation: PIN, RAFIX, DOWEL, MINIFIX"
IS_AVAILABLE_ASHELF_OVERALL_PIN_OP -> "Perform PIN operation to overall of side panels"
ASHELF_OVERALL_PIN_OP_LOWER_BEGINNING_ELEV -> "Lower beginning elevation of over all pin operation to adjustable shelf"
ASHELF_OVERALL_PIN_OP_UPPER_ENDING_LIMIT -> "Upper ending limit from side panels upper edge of overall pin operation to adjustable shelf"


; M?ddle hole group parameters
LIMIT_FOR_MIDDLE_HOLE_GROUP -> "Required adjustable shelf height for middle hole group"
MIDDLE_HOLE_GROUP_OFFSET_FROM_CENTER_OF_SIDE -> "Offset from middle hole group to midpoint of adjustable shelf height"
IS_MIDDLE_HOLE_GROUP_AVAILABLE -> "Middle hole group is available"

ASHELF_HOLES_ON_LOWER_SURFACE -> "Holes will be performed on lower surface of adjustable shelves"

; Adjustable shelves parameters
SHELVES_MAIN_ROTATION -> "Main rotation for shelves"
SHELVES_CUTLIST_ROTATION -> "Grain rotation of cutlist for shelves"
SHELVES_DXF_ROTATION -> "Dxf rotation of dxf output for shelves"
SHELVES_MIRRORING_AXIS -> "Axis of mirroring operation for shelves"

SHELVES_LABEL -> "Label of shelves"
SHELVES_TAG -> "Tag of shelves"

SHELVES_LEFT_EDGESTRIP_MAT -> "Left edgestrip material of shelves"
SHELVES_RIGHT_EDGESTRIP_MAT -> "Right edgestrip material of shelves"
SHELVES_TOP_EDGESTRIP_MAT -> "Top edgestrip material of shelves"
SHELVES_BOTTOM_EDGESTRIP_MAT -> "Bottom edgestrip material of shelves"

ASHELF_THICKNESS -> "Thickness of general unit adjustable shelf"

; Dividers parameters
DIVIDER_FRONT_OFFSET -> "Front clearance for dividers"

DIVIDERS_MAIN_ROTATION -> "Main rotation for dividers"
DIVIDERS_CUTLIST_ROTATION -> "Grain rotation of cutlist for dividers"
DIVIDERS_DXF_ROTATION -> "Dxf rotation of dxf output for dividers"
DIVIDERS_MIRRORING_AXIS -> "Axis of mirroring operation for dividers"

DIVIDERS_LABEL -> "Label of dividers"
DIVIDERS_TAG -> "Tag of dividers"

DIVIDERS_LEFT_EDGESTRIP_MAT -> "Left edgestrip material of dividers"
DIVIDERS_RIGHT_EDGESTRIP_MAT -> "Right edgestrip material of dividers"
DIVIDERS_TOP_EDGESTRIP_MAT -> "Top edgestrip material of dividers"
DIVIDERS_BOTTOM_EDGESTRIP_MAT -> "Bottom edgestrip material of dividers"

DIVIDERS_THICKNESS -> "Thickness of divider"

; Corner unit adjustable shelf parameters
AK_ASHELF_FRONT_HOLE_OFFSET -> "Offset from front hole to frontside of corner unit adjustable shelf"
AK_ASHELF_BACK_HOLE_OFFSET -> "Offset from back hole to backside of corner unit adjustable shelf"

AK_ASHELF_MAIN_ROTATION -> "Main rotation of adjustable shelf"
AK_ASHELF_CUTLIST_ROTATION -> "Grain rotation of cutlist of adjustable shelf"
AK_ASHELF_DXF_ROTATION -> "Dxf rotation of dxf output of adjustable shelf"
AK_ASHELF_MIRRORING_AXIS -> "Axis of mirroring operation of adjustable shelf"

AK_ASHELF_LABEL -> "Label of adjustable shelf"
AK_ASHELF_TAG -> "Tag of adjustable shelf"

AK_ASHELF_WID_EDGESTRIP_MAT -> "Material of edgestrip on adjustable shelf width, WID"
AK_ASHELF_HEI_EDGESTRIP_MAT -> "Material of edgestrip on adjustable shelf height, HEI"
AK_ASHELF_DEP_EDGESTRIP_MAT -> "Material of edgestrip on adjustable shelf depth, DEP"
AK_ASHELF_WIDOPPWID_EDGESTRIP_MAT -> "Material of edgestrip on edge which is opposite of adjustable shelf width, WIDOPPWID"
AK_ASHELF_WIDOPPHEI_EDGESTRIP_MAT -> "Material of edgestrip on edge which is opposite of adjustable shelf height, WIDOPPHEI"
AK_ASHELF_DEP2_EDGESTRIP_MAT -> "Material of edgestrip on adjustable shelf second depth, DEP2"
AK_ASHELF_SLOPING_EDGESTRIP_MAT -> "Material of edgestrip on adjustable shelf sloping edge"

AK_ASHELF_THICKNESS -> "Thickness of corner unit adjustable shelf"

;Fridge cabinet back panel parameters
BC_ASHELF_FRONT_HOLE_OFFSET -> "Offset from front hole to frontside of Fridge cabinet adjustable shelf"
BC_ASHELF_BACK_HOLE_OFFSET -> "Offset from back hole to backside of Fridge cabinet adjustable shelf"

BC_ASHELF_PANEL_MAIN_ROTATION -> "Main rotation"
BC_ASHELF_PANEL_CUTLIST_ROTATION -> "Grain rotation of cutlist"
BC_ASHELF_PANEL_DXF_ROTATION -> "Dxf rotation of dxf output"
BC_ASHELF_PANEL_MIRRORING_AXIS -> "Axis of mirroring operation"

BC_ASHELF_PANEL_LABEL -> "Label of panel"
BC_ASHELF_PANEL_TAG -> "Tag of panel"

BC_ASHELF_LEFT_EDGESTRIP_MAT -> "Material of left edgestrip"
BC_ASHELF_RIGHT_EDGESTRIP_MAT -> "Material of right edgestrip"
BC_ASHELF_TOP_EDGESTRIP_MAT -> "Material of top edgestrip"
BC_ASHELF_BOTTOM_EDGESTRIP_MAT -> "Material of bottom edgestrip"

BC_ASHELF_THICKNESS -> "Thickness of refrigerator unit adjustable shelf"

;Oblique unit back panel parameters
UE_ASHELF_FRONT_HOLE_OFFSET -> "Offset from front hole to frontside of oblique unit adjustable shelf"
UE_ASHELF_BACK_HOLE_OFFSET -> "Offset from back hole to backside of oblique unit adjustable shelf"

UE_ASHELF_PANEL_MAIN_ROTATION -> "Main rotation"
UE_ASHELF_PANEL_CUTLIST_ROTATION -> "Grain rotation of cutlist"
UE_ASHELF_PANEL_DXF_ROTATION -> "Dxf rotation of dxf output"
UE_ASHELF_PANEL_MIRRORING_AXIS -> "Axis of mirroring operation"

UE_ASHELF_PANEL_LABEL -> "Label of panel"
UE_ASHELF_PANEL_TAG -> "Tag of panel"

UE_ASHELF_LEFT_EDGESTRIP_MAT -> "Material of left edgestrip"
UE_ASHELF_RIGHT_EDGESTRIP_MAT -> "Material of right edgestrip"
UE_ASHELF_TOP_EDGESTRIP_MAT -> "Material of top edgestrip"
UE_ASHELF_BOTTOM_EDGESTRIP_MAT -> "Material of bottom edgestrip"

UE_ASHELF_THICKNESS -> "Thickness of oblique unit adjustable shelf"

SHELF_PIN_CODE -> "Code of shelf pins"
SHELF_PIN_UNIT -> "Unit of shelf pins"
QUANTITY_OF_SHELF_PINS -> "Number of pins on each adjustable shelf"

;prefvars
; (if (null ASHELF_AND_DIVIDER_DEFAULT_LOADED)
	; (progn
		; General adjustable shelf parameters
		(_SETA 'ASHELF_FRONT_OFFSET 20)
		(_SETA 'ASHELF_BACK_OFFSET 0)
		(_SETA 'ASHELF_SIDES_TOTAL_OFFSET 1)
		(_SETA 'GLASS_SHELF_THICKNESS 5)
		
		(_SETA 'ASHELF_HOLE_DIAMETER 5.0)
		(_SETA 'ASHELF_HOLE_DEPTH 13)
		(_SETA 'SHELF_HOLES_EXTRA_UP 1)
		(_SETA 'SHELF_HOLES_EXTRA_DOWN 1)
		(_SETA 'HOLE_GROUP_DISTANCE 32)
		(_SETA 'ASHELF_BACK_HOLE_OFFSET 50)
		(_SETA 'ASHELF_FRONT_HOLE_OFFSET 50)
		
		(_SETA 'ASHELF_OPERATION "PIN")
		
		;Overall pin operation parameters
		(_SETA 'IS_AVAILABLE_ASHELF_OVERALL_PIN_OP nil)
		(_SETA 'ASHELF_OVERALL_PIN_OP_LOWER_BEGINNING_ELEV 150)
		(_SETA 'ASHELF_OVERALL_PIN_OP_UPPER_ENDING_LIMIT 150)
		
		;pin conn hole on adjustable shelf parameters
		(_SETA 'IS_AVAILABLE_ASHELF_PIN_CONN_HOLE T)
		(_SETA 'ASHELF_PIN_CONN_HOLE_SIDE_OFFSET 9)
		(_SETA 'ASHELF_PIN_CONN_HOLE_DIAMETER 5)
		(_SETA 'ASHELF_PIN_CONN_HOLE_DEPTH 12)

		; M?ddle hole group parameters
		(_SETA 'LIMIT_FOR_MIDDLE_HOLE_GROUP 680)
		(_SETA 'MIDDLE_HOLE_GROUP_OFFSET_FROM_CENTER_OF_SIDE 0)
		(_SETA 'IS_MIDDLE_HOLE_GROUP_AVAILABLE nil)
		; Operation surface
		(_SETA 'ASHELF_HOLES_ON_LOWER_SURFACE nil)
		
		; Adjustable shelves parameters
		(_SETA 'SHELVES_MAIN_ROTATION 90)
		(_SETA 'SHELVES_CUTLIST_ROTATION nil)
		(_SETA 'SHELVES_DXF_ROTATION nil)
		(_SETA 'SHELVES_MIRRORING_AXIS nil)

		(_SETA 'SHELVES_LABEL "SHELVES")
		(_SETA 'SHELVES_TAG nil)

		(_SETA 'SHELVES_LEFT_EDGESTRIP_MAT nil)
		(_SETA 'SHELVES_RIGHT_EDGESTRIP_MAT nil)
		(_SETA 'SHELVES_TOP_EDGESTRIP_MAT nil)
		(_SETA 'SHELVES_BOTTOM_EDGESTRIP_MAT nil)
		
		(_SETA 'ASHELF_THICKNESS __AD_PANELTHICK)
		
		; Dividers parameters
		(_SETA 'DIVIDER_FRONT_OFFSET 0)
		
		(_SETA 'DIVIDERS_MAIN_ROTATION 90)
		(_SETA 'DIVIDERS_CUTLIST_ROTATION nil)
		(_SETA 'DIVIDERS_DXF_ROTATION nil)
		(_SETA 'DIVIDERS_MIRRORING_AXIS nil)

		(_SETA 'DIVIDERS_LABEL "DIVIDER")
		(_SETA 'DIVIDERS_TAG nil)

		(_SETA 'DIVIDERS_LEFT_EDGESTRIP_MAT nil)
		(_SETA 'DIVIDERS_RIGHT_EDGESTRIP_MAT nil)
		(_SETA 'DIVIDERS_TOP_EDGESTRIP_MAT nil)
		(_SETA 'DIVIDERS_BOTTOM_EDGESTRIP_MAT nil)
		
		(_SETA 'DIVIDERS_THICKNESS __AD_PANELTHICK)
		
		; Corner unit adjustable shelf parameters
		(_SETA 'AK_ASHELF_FRONT_HOLE_OFFSET 30)
		(_SETA 'AK_ASHELF_BACK_HOLE_OFFSET 30)
		
		(_SETA 'AK_ASHELF_MAIN_ROTATION nil)
		(_SETA 'AK_ASHELF_CUTLIST_ROTATION nil)
		(_SETA 'AK_ASHELF_DXF_ROTATION nil)
		(_SETA 'AK_ASHELF_MIRRORING_AXIS nil)
                
		(_SETA 'AK_ASHELF_LABEL "SHELVES")
		(_SETA 'AK_ASHELF_TAG nil)

		(_SETA 'AK_ASHELF_WID_EDGESTRIP_MAT nil)
		(_SETA 'AK_ASHELF_HEI_EDGESTRIP_MAT nil)
		(_SETA 'AK_ASHELF_DEP_EDGESTRIP_MAT nil)
		(_SETA 'AK_ASHELF_WIDOPPWID_EDGESTRIP_MAT nil)
		(_SETA 'AK_ASHELF_WIDOPPHEI_EDGESTRIP_MAT nil)
		(_SETA 'AK_ASHELF_DEP2_EDGESTRIP_MAT nil)
		(_SETA 'AK_ASHELF_SLOPING_EDGESTRIP_MAT nil)
		
		(_SETA 'AK_ASHELF_THICKNESS __AD_PANELTHICK)
		
		;Fridge cabinet back panel parameters
		(_SETA 'BC_ASHELF_FRONT_HOLE_OFFSET 32)
		(_SETA 'BC_ASHELF_BACK_HOLE_OFFSET 32)
		
		(_SETA 'BC_ASHELF_PANEL_MAIN_ROTATION nil)
		(_SETA 'BC_ASHELF_PANEL_CUTLIST_ROTATION nil)
		(_SETA 'BC_ASHELF_PANEL_DXF_ROTATION nil)
		(_SETA 'BC_ASHELF_PANEL_MIRRORING_AXIS nil)
                
		(_SETA 'BC_ASHELF_PANEL_LABEL "SHELVES")
		(_SETA 'BC_ASHELF_PANEL_TAG nil)
                
		(_SETA 'BC_ASHELF_LEFT_EDGESTRIP_MAT nil)
		(_SETA 'BC_ASHELF_RIGHT_EDGESTRIP_MAT nil)
		(_SETA 'BC_ASHELF_TOP_EDGESTRIP_MAT nil)
		(_SETA 'BC_ASHELF_BOTTOM_EDGESTRIP_MAT nil)
		
		(_SETA 'BC_ASHELF_THICKNESS __AD_PANELTHICK)
		
		;Oblique unit back panel parameters
		(_SETA 'UE_ASHELF_FRONT_HOLE_OFFSET 20)
		(_SETA 'UE_ASHELF_BACK_HOLE_OFFSET 0)

		(_SETA 'UE_ASHELF_PANEL_MAIN_ROTATION nil)
		(_SETA 'UE_ASHELF_PANEL_CUTLIST_ROTATION nil)
		(_SETA 'UE_ASHELF_PANEL_DXF_ROTATION nil)
		(_SETA 'UE_ASHELF_PANEL_MIRRORING_AXIS nil)

		(_SETA 'UE_ASHELF_PANEL_LABEL "SHELVES")
		(_SETA 'UE_ASHELF_PANEL_TAG nil)

		(_SETA 'UE_ASHELF_LEFT_EDGESTRIP_MAT nil)
		(_SETA 'UE_ASHELF_RIGHT_EDGESTRIP_MAT nil)
		(_SETA 'UE_ASHELF_TOP_EDGESTRIP_MAT nil)
		(_SETA 'UE_ASHELF_BOTTOM_EDGESTRIP_MAT nil)
		
		(_SETA 'UE_ASHELF_THICKNESS __AD_PANELTHICK)
		
		(_SETA 'SHELF_PIN_CODE "SHELF_PIN")
		(_SETA 'SHELF_PIN_UNIT "pc")
		; This variables can not be changed by user on interface of part2cam
		(if (null SHELF_PIN_UNIT) (_FSET (_ 'SHELF_PIN_UNIT "pc")))
		(if (null QUANTITY_OF_SHELF_PINS) (_FSET (_ 'QUANTITY_OF_SHELF_PINS 4)))
		
		(_FSET (_ 'ASHELF_SIDE_OFFSET (/ ASHELF_SIDES_TOTAL_OFFSET 2.0)))
		
		; CONTINGENCIES
		; ASHELF_HOLES_ON_LOWER_SURFACE
		(if (null (member ASHELF_HOLES_ON_LOWER_SURFACE (_ nil T))) (_FSET (_ 'ASHELF_HOLES_ON_LOWER_SURFACE T)))
		; IS_MIDDLE_HOLE_GROUP_AVAILABLE
		(if (null (member IS_MIDDLE_HOLE_GROUP_AVAILABLE (_ nil T))) (_FSET (_ 'IS_MIDDLE_HOLE_GROUP_AVAILABLE T)))
		
		; (_FSET (_ 'ASHELF_AND_DIVIDER_DEFAULT_LOADED T))
	; )
; )
(_NONOTCH)
