; Manufacturing choices
NO_NEED_HINGE_HOLES_ON_DOORS -> "Hinge holes are not performed on doors"
NO_NEED_HINGE_CONN_HOLES ->  "Hinge connection holes are not performed"

HINGE_SCREW_CODE -> "Hinge screw code"
HINGE_SCREW_UNIT -> "Hinge screw unit"
DO_NOT_ADD_HINGE_SCREW_AS_ITEM -> "Do not add hinge screw as item"

NEED_HINGE_CONN_HOLES_ON_TWO_SIDES -> "Hinge connection holes are performed on two sides"
CHANGE_HINGE_OFFSETS_ON_DOOR_WITH_LOWER_VARIANCES -> "Change door hinge offsets according to right or left lower variances"

; Standard hinge variables
HINGE_OFFSET -> "Hinge hole offset from connecting edge"
HINGE_DISTANCE -> "Hinge hole offset from side edges of connecting face"
BIG_HOLE_DIAMETER -> "Diameter of big hinge hole"
BIG_HOLE_DEPTH -> "Depth of big hinge hole"
BIG_TO_SMALLS_OFFSET_X -> "Offset from big hole center to small hole center"
SMALL_TO_SMALL_OFFSET -> "Offset between center of small holes"
SMALL_HOLES_DIAMETER -> "Diameter of small hinge holes"
SMALL_HOLES_DEPTH -> "Depth of small hinge holes"

HINGE1_CODE -> "First hinge code"
HINGE2_CODE -> "Second hinge code"
HINGE1_QUAN -> "Quantity of standard hinges for one door"
HINGE2_QUAN -> "Quantity of standard hinges with stop for one door"

; Connection holes variables
HINGE_CONNECTION_HOLES_OFFSET -> "Offset of hinge connection holes to edge"
HINGE_CONNECTION_HOLES_DISTANCE -> "Distance between hinge connection holes"
HINGE_CONNECTION_HOLES_DIAMETER -> "Diameter of hinge connection holes"
HINGE_CONNECTION_HOLES_DEPTH -> "Depth of hinge connection holes"
ARE_CONNECTION_HOLES_HORIZONTAL -> "Perform hinge connection holes horizontally"

; Extra hinge variables
USE_EXTRA_HINGE -> "Perform extra hinges to door"
EXTRA_HINGES_PARAMS_LIST -> "Extra hinges parameters list -> (_ (_ HINGE_CODE EXTRA_HINGE_CENTER_OFFSET OFFSET_POINT) ...) Ex: (_ (_ \"HINGE_STANDARD\" -100 \"MIDDLE\") ...) [OFFSET_POINT -> MIDDLE BOTTOM TOP]"

; Other hinge parameters
CU_HINGES_ON_ONE_DOOR -> "Hinge holes are gathered on one door when corner unit doors are connected to each other with hinge"
SECONDARY_HINGE_OFFSET -> "Passive door hinge hole offset from active door side egde for corner unit"
SECONDARY_BIG_TO_SMALLS_OFFSET_X -> "Offset from big hole center to small hole center for active door side hinge of corner unit passive door"
SECONDARY_HINGE_CONNECTION_HOLES_OFFSET -> "Offset of hinge connection holes to corner unit active door side edge of passive door"

A1K9090_HINGE1_CODE -> "For A1K9090 units, door to body hinge code"
A1K9090_HINGE2_CODE -> "For A1K9090 units, door to door hinge code"
A1K9090_HINGE1_QUAN -> "For A1K9090 units, quantity of door to body hinges for one door"
A1K9090_HINGE2_QUAN -> "For A1K9090 units, quantity of door to door hinges for one door"

A2K9090_HINGE_CODE -> "For A2K9090 units, hinge code"
A2K9090_HINGE_QUAN -> "For A2K9090 units, quantity of hinges for one door"

USE_AKK_EXTRA_HINGE -> "Perform extra hinge to doors of blind corner units"
EXTRA_AKK_HINGES_PARAMS_LIST -> "Extra hinge parameters for blind corner units -> (_ (_ HINGE_CODE EXTRA_HINGE_CENTER_OFFSET OFFSET_POINT) ...) Ex: (_ (_ \"HINGE_STANDARD\" -100 \"MIDDLE\") ...) [OFFSET_POINT -> MIDDLE BOTTOM TOP]"
WHICH_AKK_DOORS_GOT_MIDDLE_HINGE_OP -> "If blind corner unit got two division, Which division doors got middle hinge operation Ex: ''UP'',''DOWN'',''BOTH''"
AKK_HINGE_CODE -> "For AKK units, hinge code"
AKK_HINGE_QUAN -> "For AKK units, quantity of hinges for one door"

HINGE_110_DEG_CODE -> "110 degree hinge code"
HINGE_110_DEG_QUAN -> "Quantity of 110 degree hinges for one door"
EXTRA_HINGE_110_DEG_QUAN -> "Quantity of extra 110 degree hinges for one door"

HINGE_UNIT -> "Unit of hinges"

HINGE_CODE_INITIAL_VALUE -> "HINGE_STANDARD"

;prefvars
; Manufacturing choices
(_SETA 'NO_NEED_HINGE_HOLES_ON_DOORS nil)
(_SETA 'NO_NEED_HINGE_CONN_HOLES nil)

(_SETA 'HINGE_SCREW_CODE "HINGE_SCREW")
(_SETA 'HINGE_SCREW_UNIT "ad")
(_SETA 'DO_NOT_ADD_HINGE_SCREW_AS_ITEM nil)


(_SETA 'NEED_HINGE_CONN_HOLES_ON_TWO_SIDES nil)
(_SETA 'CHANGE_HINGE_OFFSETS_ON_DOOR_WITH_LOWER_VARIANCES nil)

; Standard hinge variables
(_SETA 'HINGE_OFFSET 22.5)
(_SETA 'HINGE_DISTANCE 50)
(_SETA 'BIG_HOLE_DIAMETER 35)
(_SETA 'BIG_HOLE_DEPTH 13)
(_SETA 'BIG_TO_SMALLS_OFFSET_X 9.5)
(_SETA 'SMALL_TO_SMALL_OFFSET 45)
(_SETA 'SMALL_HOLES_DIAMETER 5)
(_SETA 'SMALL_HOLES_DEPTH 13)

(_SETA 'HINGE1_CODE "75T1550_107_DERECE_DUZ_MENTESE")
(_SETA 'HINGE2_CODE "HINGE_STOP")
(_SETA 'HINGE1_QUAN 2)
(_SETA 'HINGE2_QUAN 0)

; Connection holes variables
(_SETA 'HINGE_CONNECTION_HOLES_OFFSET 20)
(_SETA 'HINGE_CONNECTION_HOLES_DISTANCE 32)
(_SETA 'HINGE_CONNECTION_HOLES_DIAMETER 5)
(_SETA 'HINGE_CONNECTION_HOLES_DEPTH 13)
(_SETA 'ARE_CONNECTION_HOLES_HORIZONTAL T)

; Extra hinge variables
(_SETA 'USE_EXTRA_HINGE T)
(_SETA 'EXTRA_HINGES_PARAMS_LIST (_ (_ "75T1550_107_DERECE_DUZ_MENTESE" 636 "TOP") (_ "75T1550_107_DERECE_DUZ_MENTESE" 758 "BOTTOM") ))

(_FSET (_ 'BIG_TO_SMALLS_OFFSET_Y (/ SMALL_TO_SMALL_OFFSET 2.0)))

; Other hinge parameters
(_SETA 'CU_HINGES_ON_ONE_DOOR nil)
(_SETA 'SECONDARY_HINGE_OFFSET 10)
(_SETA 'SECONDARY_BIG_TO_SMALLS_OFFSET_X 9.5)
(_SETA 'SECONDARY_HINGE_CONNECTION_HOLES_OFFSET 40)


(_SETA 'A1K9090_HINGE1_CODE "HINGE_165_DEG")
(_SETA 'A1K9090_HINGE2_CODE "HINGE_115_DEG")
(_SETA 'A1K9090_HINGE1_QUAN 2)
(_SETA 'A1K9090_HINGE2_QUAN 2)

(_SETA 'A2K9090_HINGE_CODE "HINGE_90_DEG")
(_SETA 'A2K9090_HINGE_QUAN 2)

(_SETA 'USE_AKK_EXTRA_HINGE nil)
(_SETA 'EXTRA_AKK_HINGES_PARAMS_LIST (_ ))
(_SETA 'WHICH_AKK_DOORS_GOT_MIDDLE_HINGE_OP "BOTH")
(_SETA 'AKK_HINGE_CODE "HINGE_165_DEG")
(_SETA 'AKK_HINGE_QUAN 2)

(_SETA 'HINGE_110_DEG_CODE "HINGE_110_DEG")
(_SETA 'HINGE_110_DEG_QUAN 2)
(_SETA 'EXTRA_HINGE_110_DEG_QUAN 1)

(_SETA 'HINGE_UNIT "pc")

; CONTINGENCIES
; HINGE_IN_MIDDLE
(if (null (member HINGE_IN_MIDDLE (_ nil T))) (_FSET (_ 'HINGE_IN_MIDDLE T)))
; CU_HINGES_ON_ONE_DOOR
(if (null (member CU_HINGES_ON_ONE_DOOR (_ nil T))) (_FSET (_ 'CU_HINGES_ON_ONE_DOOR T)))
; NO_NEED_HINGE_HOLES_ON_DOORS
(if (null (member NO_NEED_HINGE_HOLES_ON_DOORS (_ nil T))) (_FSET (_ 'NO_NEED_HINGE_HOLES_ON_DOORS T)))
; NO_NEED_HINGE_CONN_HOLES
(if (null (member NO_NEED_HINGE_CONN_HOLES (_ nil T))) (_FSET (_ 'NO_NEED_HINGE_CONN_HOLES T)))
; IS_CONNECTION_HOLES_HORIZONTAL
(if (null (member ARE_CONNECTION_HOLES_HORIZONTAL (_ nil T))) (_FSET (_ 'ARE_CONNECTION_HOLES_HORIZONTAL T)))

; Standard Values, NO_NEED_HINGE_HOLES_ON_DOORS nil, NO_NEED_HINGE_CONN_HOLES nil
; Standard Values, HINGE_OFFSET 23, HINGE_DISTANCE 100, BIG_HOLE_DIAMETER 35, BIG_HOLE_DEPTH 12, BIG_TO_SMALLS_OFFSET_X 9.5, SMALL_TO_SMALL_OFFSET 45, SMALL_HOLES_DIAMETER 8, SMALL_HOLES_DEPTH 12
; Standard Values, HINGE_CONNECTION_HOLES_OFFSET 37, HINGE_CONNECTION_HOLES_DISTANCE 32, HINGE_CONNECTION_HOLES_DIAMETER 5, HINGE_CONNECTION_HOLES_DEPTH 1, ARE_CONNECTION_HOLES_HORIZONTAL nil
; Standard Values, HINGE_CODE HINGE_STANDARD, HINGE_UNIT pc

;extra hinge list recreate
(if __CURDIVDOORSLIST
	(if EXTRA_HINGES_PARAMS_LIST
		(progn
			(_FSET (_ 'paramBody (_& (_ "CDOOR_" __CURDIVORDER "_" 1 "_"))))
			(_FSET (_ 'currentDoorFRAME (_S2V (_& (_ paramBody "FRAME")))))
			(_FSET (_ 'topSideFrameVAL (getnth 0 currentDoorFRAME)))
			(_FSET (_ 'bottomSideFrameVAL (getnth 2 currentDoorFRAME)))

			(_FSET (_ 'tempExtraHingeParamList EXTRA_HINGES_PARAMS_LIST))
			(_FSET (_ 'EXTRA_HINGES_PARAMS_LIST nil))
			
			(foreach hingee tempExtraHingeParamList
				(cond
					((equal (getnth 2 hingee) "MIDDLE")
						(_FSET (_ 'EXTRA_HINGES_PARAMS_LIST (append EXTRA_HINGES_PARAMS_LIST (_ (_ (getnth 0 hingee) (getnth 1 hingee))))))
					)
					((equal (getnth 2 hingee) "BOTTOM")
						(_FSET (_ 'EXTRA_HINGES_PARAMS_LIST (append EXTRA_HINGES_PARAMS_LIST (_ (_ (getnth 0 hingee)  (+ (/ (- (cadr (car __CURDIVDOORSLIST)) topSideFrameVAL bottomSideFrameVAL) -2) (getnth 1 hingee)))))))
					)
					((equal (getnth 2 hingee) "TOP")
						(_FSET (_ 'EXTRA_HINGES_PARAMS_LIST (append EXTRA_HINGES_PARAMS_LIST (_ (_ (getnth 0 hingee)  (- (/ (- (cadr (car __CURDIVDOORSLIST)) topSideFrameVAL bottomSideFrameVAL) 2) (getnth 1 hingee)))))))
					)
				)
			)
		)
	)
)
;extra hinge list recreate
(if __CURDIVDOORSLIST
	(if EXTRA_AKK_HINGES_PARAMS_LIST
		(progn
			(_FSET (_ 'paramBody (_& (_ "AKK_CDOOR_" 1 "_"))))
			(_FSET (_ 'currentDoorFRAME (_S2V (_& (_ paramBody "FRAME")))))
			(_FSET (_ 'topSideFrameVAL (getnth 0 currentDoorFRAME)))
			(_FSET (_ 'bottomSideFrameVAL (getnth 2 currentDoorFRAME)))

			(_FSET (_ 'tempExtraHingeParamList EXTRA_AKK_HINGES_PARAMS_LIST))
			(_FSET (_ 'EXTRA_AKK_HINGES_PARAMS_LIST nil))
			
			(foreach hingee tempExtraHingeParamList
				(cond
					((equal (getnth 2 hingee) "MIDDLE")
						(_FSET (_ 'EXTRA_AKK_HINGES_PARAMS_LIST (append EXTRA_AKK_HINGES_PARAMS_LIST (_ (_ (getnth 0 hingee) (getnth 1 hingee))))))
					)
					((equal (getnth 2 hingee) "BOTTOM")
						(_FSET (_ 'EXTRA_AKK_HINGES_PARAMS_LIST (append EXTRA_AKK_HINGES_PARAMS_LIST (_ (_ (getnth 0 hingee)  (+ (/ (-  (cadr (car __CURDIVDOORSLIST)) topSideFrameVAL bottomSideFrameVAL) -2) (getnth 1 hingee)))))))
					)
					((equal (getnth 2 hingee) "TOP")
						(_FSET (_ 'EXTRA_AKK_HINGES_PARAMS_LIST (append EXTRA_AKK_HINGES_PARAMS_LIST (_ (_ (getnth 0 hingee)  (- (/ (- (cadr (car __CURDIVDOORSLIST))  topSideFrameVAL bottomSideFrameVAL) 2) (getnth 1 hingee)))))))
					)
				)
			)
		)
	)
)

; PRELIMINARY CONTROLS
(if (null HINGE_PRELIMINARY_CONTROLS)
	(progn
		; Controls for connection holes
		(if (null ARE_CONNECTION_HOLES_HORIZONTAL)
			(progn
				; Connection holes are performed VERTICALLY
				(_FSET (_ 'HINGE_PARAM_VER (/ HINGE_CONNECTION_HOLES_DISTANCE 2.0)))
				(_FSET (_ 'HINGE_PARAM_HOR 0))
			)
			(progn
				; Connection holes are performed HORIZONTALLY
				(_FSET (_ 'HINGE_PARAM_VER 0))
				(_FSET (_ 'HINGE_PARAM_HOR HINGE_CONNECTION_HOLES_DISTANCE))
			)
		)
		; One-and-done
		(_FSET (_ 'HINGE_PRELIMINARY_CONTROLS T))
	)
)
; These variables are set by SETA function in operation recipes. Thats why they have to put out of one time load package
(setq HINGE_CODES (list HINGE1_CODE HINGE2_CODE))
(setq QUANTITY_PARAMS (list (list HINGE1_QUAN HINGE_UNIT) (list HINGE2_QUAN HINGE_UNIT)))
(setq QUANTITY_PARAMS_EXTRA (list EXTRA_HINGE_QUAN HINGE_UNIT))

; This variable which is below can not be changed by users from part2cam interfaces
(if (null HINGE_UNIT) (_FSET (_ 'HINGE_UNIT "pc")))
(_NONOTCH)
