; States of hinge connection
; Modul Direction		Door Open Direction			Panel which hinge connects to
;		Left					Left					Extra Part or Blind Door
;		Left					Right				 		Right Panel
;		Right					Left						Left Panel
;		Right					Right					Extra Part or Blind Door

(if (> __NOTCHDIM1 __AD_PANELTHICK)
	(progn
		(if (_NOTNULL USE_AKK_EXTRA_HINGE)
			(progn
				(if (or (_NOTNULL USE_PROJECT_PRICING_RECIPES) (null BLIND_CORNER_CONTROL))
					(progn
						; If project pricing recipes will be used instead of operations or module type validation fails then there is no need for notch operations also
						(_NONOTCH)
					)
					(progn
						(_FSET (_ 'distOfConnHolesToCenter HINGE_PARAM_VER))
						(_FSET (_ 'connHolesHorDiff HINGE_PARAM_HOR))
						
						(_FSET (_ 'doorCounter 0))
						(repeat __DOORSCOUNT
							(_FSET (_ 'doorCounter (+ 1 doorCounter)))
							(_FSET (_ 'paramBody (_& (_ "AKK_CDOOR_" doorCounter "_"))))
							
							(_FSET (_ 'currentDoorTYPE (_S2V (_& (_ paramBody "TYPE")))))
							; Control for drawer
							(if (and (_ISDOORDRAWER currentDoorTYPE) (null ASSUME_DRAWER_AS_DOOR))
								(_FSET (_ 'drawerControl nil))
								(_FSET (_ 'drawerControl T))
							)
							
							(_FSET (_ 'currentDoorCODE (_CREATEDOORCODE currentDoorTYPE __CURDIVORDER doorCounter CHANGE_BCU_DOOR_CODES "BCU")))
							
							;blind corner door spesific controls
							(cond
								((equal __DOORSCOUNT 1)
									(_FSET (_ 'isAvailableAkkExHinge T))
								)
								((equal __DOORSCOUNT 2)
									(cond 
										((equal doorCounter 1)
											(if	(_ISDOORDRAWER currentDoorTYPE)
												(progn
													(if (and ASSUME_DRAWER_AS_DOOR (member WHICH_AKK_DOORS_GOT_MIDDLE_HINGE_OP (_ "BOTH" "UP")))
														(_FSET (_ 'isAvailableAkkExHinge T))
														(_FSET (_ 'isAvailableAkkExHinge nil))
													)
													(_FSET (_ 'firstDoorIsDrawer T))
												)
												(progn
													(_FSET (_ 'isAvailableAkkExHinge T))
													(_FSET (_ 'firstDoorIsDrawer nil))
												)
											)
										)
										((equal doorCounter 2)
											(if firstDoorIsDrawer
												(progn
													(if (member WHICH_AKK_DOORS_GOT_MIDDLE_HINGE_OP (_ "BOTH" "DOWN"))
													(_FSET (_ 'isAvailableAkkExHinge T))
													(_FSET (_ 'isAvailableAkkExHinge nil))
													)
												)
												(_FSET (_ 'isAvailableAkkExHinge T))
											)
										)
									)
								)
								((equal __DOORSCOUNT 3)
									(cond 
										((equal doorCounter 1)
											(if (and ASSUME_DRAWER_AS_DOOR (member WHICH_AKK_DOORS_GOT_MIDDLE_HINGE_OP (_ "BOTH" "UP")))
												(_FSET (_ 'isAvailableAkkExHinge T))
												(_FSET (_ 'isAvailableAkkExHinge nil))
											)
										)
										(T
											(if (member WHICH_AKK_DOORS_GOT_MIDDLE_HINGE_OP (_ "BOTH" "DOWN"))
												(_FSET (_ 'isAvailableAkkExHinge T))
												(_FSET (_ 'isAvailableAkkExHinge nil))
											)
										)
									)
								)
							)
							; If drawer control fails, no part of operation is performed
							(if isAvailableAkkExHinge
								(progn
									; Current door variables
									(_FSET (_ 'currentDoorHEI (_S2V (_& (_ paramBody "HEI")))))
									(_FSET (_ 'currentDoorWID (_S2V (_& (_ paramBody "WID")))))
									(_FSET (_ 'currentDoorELEV (_S2V (_& (_ paramBody "ELEV")))))
									(_FSET (_ 'currentDoorODIR (_S2V (_& (_ paramBody "ODIR")))))
									(_FSET (_ 'currentDoorFRAME (_S2V (_& (_ paramBody "FRAME")))))
									(if (not (apply 'OR currentDoorFRAME))
										(progn
											(_FSET (_ 'topFrameVAL 0))
											(_FSET (_ 'rightFrameVAL 0))
											(_FSET (_ 'bottomFrameVAL 0))
											(_FSET (_ 'leftFrameVAL 0))
										)
										(progn
											(_FSET (_ 'topFrameVAL (getnth 0 currentDoorFRAME)))
											(_FSET (_ 'rightFrameVAL (getnth 1 currentDoorFRAME)))
											(_FSET (_ 'bottomFrameVAL (getnth 2 currentDoorFRAME)))
											(_FSET (_ 'leftFrameVAL (getnth 3 currentDoorFRAME)))
										)
									)
									
									; Extra hinge list 
									(foreach hingeItem EXTRA_AKK_HINGES_PARAMS_LIST
										(_FSET (_ 'MIDDLE_HINGE_CENTER_OFFSET (cadr hingeItem)))
										(_FSET (_ 'QUANTITY_PARAMS_EXTRA (_ 1 HINGE_UNIT)))
										(_FSET (_ 'EXTRA_HINGE_CODE (car hingeItem)))
										(_FSET (_ 'HINGE_DISTANCE (- (/ currentDoorHEI 2.0) MIDDLE_HINGE_CENTER_OFFSET)))
	
										; SIDE OPERATION
										(if (null NO_NEED_HINGE_CONN_HOLES)
											(progn
												(_FSET (_ 'holeCodeBody "HINGE_CONN_HOLE_"))
												(if (not (equal __MODULDIRECTION currentDoorODIR))
													(progn
														; Open direction of current door is different from module direction
														; Modul Direction		Door Open Direction			Panel which hinge connects to
														;		Left					Right				 		Right Panel
														;		Right					Left						Left Panel
														(if (equal currentDoorODIR "L")
															(progn
																(if (_EXISTPANEL NOTCHED_LEFT_PANEL_CODE)
																	(progn
																		(_FSET (_ 'connHolePOS_MF (_ HINGE_CONNECTION_HOLES_OFFSET (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + bottomFrameVAL - HINGE_DISTANCE + distOfConnHolesToCenter") 0)))
																		(_FSET (_ 'connHolePOS_MS (_ (+ HINGE_CONNECTION_HOLES_OFFSET connHolesHorDiff) (_= "currentDoorELEV - bottomSideStyleV1 + currentDoorHEI + bottomFrameVAL - HINGE_DISTANCE - distOfConnHolesToCenter") 0)))
																		
																		(_HOLE (_& (_ holeCodeBody "UF")) NOTCHED_LEFT_PANEL_CODE (_ connHolePOS_MF HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
																		(_HOLE (_& (_ holeCodeBody "US")) NOTCHED_LEFT_PANEL_CODE (_ connHolePOS_MS HINGE_CONNECTION_HOLES_DIAMETER HINGE_CONNECTION_HOLES_DEPTH))
																	)
																)
															)
														)
													)
												)
											)
										)
									)
								)
							)
						)
					)
				)
			)
		)
	)
)