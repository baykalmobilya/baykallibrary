; -------------------------------------------------- DEFAULT RECIPES ---------------------------------------------------------

(_RUNDEFAULTHELPERRCP "AK_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "LEFT_PANEL_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "EDGES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "GOLA_DEFAULT" nil "p2chelper")

(if (null CORNER_UNIT_CONTROL)
	(progn
		; There is no need to perform notch operations
		(_NONOTCH)
	)
	(progn
; -------------------------------------------------- EDGESTRIPS ---------------------------------------------------------
		; Edgestrip material controls
		(if (null AK_LEFT_VISIBLE_EDGESTRIP_MAT) (_FSET (_ 'AK_LEFT_VISIBLE_EDGESTRIP_MAT FRONTSIDE_EDGES_MAT)))
		(if (null AK_LEFT_HIDDEN_EDGESTRIP_MAT) (_FSET (_ 'AK_LEFT_HIDDEN_EDGESTRIP_MAT WALLSIDE_EDGES_MAT)))
		;(if (null AK_LEFT_TOP_EDGESTRIP_MAT) (_FSET (_ 'AK_LEFT_TOP_EDGESTRIP_MAT UPCAST_EDGES_MAT)))
		(if (null AK_LEFT_TOP_EDGESTRIP_MAT) (_FSET (_ 'AK_LEFT_TOP_EDGESTRIP_MAT (if (equal TOPSIDE_STYLE "TOP_ON_SIDES") nil UPCAST_EDGES_MAT))))
		(if (null AK_LEFT_BOTTOM_EDGESTRIP_MAT) (_FSET (_ 'AK_LEFT_BOTTOM_EDGESTRIP_MAT (if (equal BOTTOMSIDE_STYLE "SIDES_ON_BOTTOM") nil SIDES_BOTTOM_EDGES_MAT))))
		
		(_ADDPARTMAT2EDGESTRIPS (_ 'AK_LEFT_VISIBLE_EDGESTRIP_MAT 'AK_LEFT_HIDDEN_EDGESTRIP_MAT 'AK_LEFT_TOP_EDGESTRIP_MAT 'AK_LEFT_BOTTOM_EDGESTRIP_MAT) __MODULBODYMAT ADD_PART_MATERIAL)
		; Width of edgestrips are taken from database
		(_FSET (_ 'VISIBLE_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB AK_LEFT_VISIBLE_EDGESTRIP_MAT)))
		(_FSET (_ 'HIDDEN_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB AK_LEFT_HIDDEN_EDGESTRIP_MAT)))
		(_FSET (_ 'TOP_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB AK_LEFT_TOP_EDGESTRIP_MAT)))
		(_FSET (_ 'BOTTOM_EDGESTRIP_WID (_GETEDGESTRIPWIDFROMDB AK_LEFT_BOTTOM_EDGESTRIP_MAT)))

; -------------------------------------------------- PANEL ROTATION ---------------------------------------------------------	
		; Control for cutlist and dxf rotation
		(if (null AK_LEFT_PANEL_CUTLIST_ROTATION) (_FSET (_ 'AK_LEFT_PANEL_CUTLIST_ROTATION __MODULBODYMATROT)))
		(if (null AK_LEFT_PANEL_DXF_ROTATION) (_FSET (_ 'AK_LEFT_PANEL_DXF_ROTATION __MODULBODYMATROT)))
		
		(_FSET (_ 'AK_LEFT_PANEL_ROT (_ AK_LEFT_PANEL_MAIN_ROTATION AK_LEFT_PANEL_CUTLIST_ROTATION AK_LEFT_PANEL_DXF_ROTATION AK_LEFT_PANEL_MIRRORING_AXIS)))
		
; -------------------------------------------------- PANEL DIMENSION DATA ---------------------------------------------------------		
		; Global variables of corner unit left panel
		(_FSET (_ 'AK_LEFT_PANEL_WID  (+ __DEP2 AK_LEFT_PANEL_FRONT_VARIANCE AK_LEFT_PANEL_BACK_VARIANCE)))
		(_FSET (_ 'AK_LEFT_PANEL_HEI (_= "__HEI - bottomSideStyleV1 - topSideStyleV1")))
		
		(_FSET (_ 'AK_LEFT_PANEL_MAT __MODULBODYMAT))
		
		(_FSET (_ 'AK_LEFT_PANEL_CODE (_& (_ (XSTR "CORNER_UNIT") "_" (XSTR "LEFT_PANEL")))))
		
		;;;;;;;;;;; GOLA_PREFERENCES;;;;;;;;;;;;;;;;
		(if	(not (null GOLA_PATTERN))
			(progn	
				(setq gCavityShift (_ 0 (_= "0 - bottomSideStyleV1") 0));;if necessary, offsetting cavity. Domestic or Euro styles for example
				(setq nCavityShift (_ 0 (_= "0 - bottomSideStyleV1") 0))
				(setq PanelInformations (_GENERATEPDATAV2 AK_LEFT_PANEL_WID 
											   AK_LEFT_PANEL_HEI 
											   0 
											   gCavityShift
											   nCavityShift
											   GOLA_CAVITY_ORDER
											   nil ))
				(setq GolaPData (nth 0 PanelInformations))
				(setq GolaEdgeStrips (mapcar '(lambda (item) 
													(if (equal item 0) (setq item (_ " " 0)))
													(if (equal item 1) (setq item (_ AK_LEFT_VISIBLE_EDGESTRIP_MAT VISIBLE_EDGESTRIP_WID)))
													(if (equal item 2) (setq item (_ AK_LEFT_TOP_EDGESTRIP_MAT TOP_EDGESTRIP_WID)))
													(if (equal item 3) (setq item (_ AK_LEFT_HIDDEN_EDGESTRIP_MAT HIDDEN_EDGESTRIP_WID)))
													(if (equal item 4) (setq item (_ AK_LEFT_BOTTOM_EDGESTRIP_MAT BOTTOM_EDGESTRIP_WID)))
													item ) (nth 1 PanelInformations)))
				(setq golaControlPoints (nth 2 PanelInformations))
				(setq golaEdgeTrimCtrlPts (nth 4 PanelInformations))
				
				; milling approach lines adding 
				(if (or (equal GOLA_DRAW_APPROACH_OUTSIDE 1) (equal GOLA_DRAW_SECONDARY_FACE_APPROACH_OUTSIDE 1))
					(progn
						(_GENERATEMILLINGAPPROACH AK_LEFT_PANEL_CODE 
												  golaControlPoints                                                      ; definition of up L , down L , U cavities control points 
												  GOLA_DRAW_APPROACH_OUTSIDE											 ; Draw top approach lines
												  GOLA_DRAW_SECONDARY_FACE_APPROACH_OUTSIDE      						 ; Draw bottom approach lies
												  GOLA_MIRROR_SFACE_GEOMETRIES											 ; Mirror bottom approach lines. it needed for biesse cam			
												  GOLA_APPROACH_DISTANCE 												 ; Desired approach distance from the outside of panels
												  GOLA_INNER_RADIUS														 ; Definition of the cavities concave corner radius
												  (_ (_ 0.0 (/ AK_LEFT_PANEL_HEI 2) 0.0) (_ 1.0 (/ AK_LEFT_PANEL_HEI 2) 0.0) )); Mirror axis
						(_GENERATEEDGEMILLING AK_LEFT_PANEL_CODE 
											  golaEdgeTrimCtrlPts 						; definition of up L , down L , U cavities control points list. firsth and last points are create the line to be cut
											  GOLA_EDGE_TRIM_GEO_OFFSET_DISTANCE		; cutoff line, outward offset
											  GOLA_EDGE_TRIM_GEO_EXTENSION_DISTANCE		; extension of offseted cutoff line from start point
											  GOLA_EDGE_TRIM_GEO_EXTENSION_DISTANCE2)	; extension of offseted cutoff line to end point
					)
				)
				
				(if (equal GOLA_DRAW_PATTERN 1)
					(progn
						(_FSET (_ 'AK_LEFT_PANEL_PDATA GolaPData))												
						(_FSET (_ 'AK_LEFT_PANEL_EDGESTRIPS  GolaEdgeStrips))
					)
				)
			)
		)	
				
		(if (or (equal AK_LEFT_PANEL_EDGESTRIPS nil) (equal AK_LEFT_PANEL_PDATA nil))
			(progn
				(_FSET (_ 'AK_LEFT_PANEL_PDATA (_GENERATEPDATA AK_LEFT_PANEL_WID AK_LEFT_PANEL_HEI)))
			
				(_FSET (_ 'AK_LEFT_PANEL_EDGESTRIPS (_ (_ AK_LEFT_VISIBLE_EDGESTRIP_MAT VISIBLE_EDGESTRIP_WID)
														(_ AK_LEFT_TOP_EDGESTRIP_MAT TOP_EDGESTRIP_WID)
														(_ AK_LEFT_HIDDEN_EDGESTRIP_MAT HIDDEN_EDGESTRIP_WID)
														(_ AK_LEFT_BOTTOM_EDGESTRIP_MAT BOTTOM_EDGESTRIP_WID))))
			)
		)			
				
; -------------------------------------------------- CREATING PANEL ---------------------------------------------------------		
		(_PANELMAIN AK_LEFT_PANEL_CODE (_ AK_LEFT_PANEL_PDATA AK_LEFT_PANEL_ROT AK_LEFT_PANEL_MAT AK_LEFT_PANEL_THICKNESS))
		
		(_CREATEEDGESTRIPS AK_LEFT_PANEL_CODE AK_LEFT_PANEL_EDGESTRIPS)
		
		(_PUTLABEL AK_LEFT_PANEL_CODE AK_LEFT_PANEL_CODE (XSTR AK_LEFT_PANEL_LABEL))
		(_PUTTAG AK_LEFT_PANEL_CODE AK_LEFT_PANEL_CODE AK_LEFT_PANEL_TAG)
	)
)