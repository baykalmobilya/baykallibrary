; -------------------------------------------------- DEFAULT RECIPES ---------------------------------------------------------

(_RUNDEFAULTHELPERRCP "AK_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "BOTTOM_PANEL_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "PANELS_JUNCTION_STYLES_DEFAULT" nil "p2chelper")
(_RUNDEFAULTHELPERRCP "EDGES_DEFAULT" nil "p2chelper")

(if (null CORNER_UNIT_CONTROL)
	(progn
		; There is no need to perform notch operations
		(_NONOTCH)
	)
	(progn
; -------------------------------------------------- EDGESTRIPS ---------------------------------------------------------
		
		; Edgestrip material controls
		(if (null AK_BOTTOM_DEP_EDGESTRIP_MAT) (_FSET (_ 'AK_BOTTOM_DEP_EDGESTRIP_MAT (if (equal BOTTOMSIDE_STYLE "BOTTOM_BETWEEN_SIDES") nil BOTTOMS_SIDE_EDGES_MAT))))
		(if (null AK_BOTTOM_DEP2_EDGESTRIP_MAT) (_FSET (_ 'AK_BOTTOM_DEP2_EDGESTRIP_MAT (if (equal BOTTOMSIDE_STYLE "BOTTOM_BETWEEN_SIDES") nil BOTTOMS_SIDE_EDGES_MAT))))
		; Width of edgestrips are taken from database
		(_FSET (_ 'WID_EDGESTRIP_WIDTH (_GETEDGESTRIPWIDFROMDB AK_BOTTOM_WID_EDGESTRIP_MAT)))
		(_FSET (_ 'HEI_EDGESTRIP_WIDTH (_GETEDGESTRIPWIDFROMDB AK_BOTTOM_HEI_EDGESTRIP_MAT)))
		(_FSET (_ 'DEP_EDGESTRIP_WIDTH (_GETEDGESTRIPWIDFROMDB AK_BOTTOM_DEP_EDGESTRIP_MAT)))
		(_FSET (_ 'DEP2_EDGESTRIP_WIDTH (_GETEDGESTRIPWIDFROMDB AK_BOTTOM_DEP2_EDGESTRIP_MAT)))		
		
; -------------------------------------------------- PANEL ROTATION ---------------------------------------------------------									  
		; Common code blocks are gathered below *
		; Control for cutlist and dxf rotation
		(if (null AK_BOTTOM_PANEL_CUTLIST_ROTATION) (_FSET (_ 'AK_BOTTOM_PANEL_CUTLIST_ROTATION __MODULBODYMATROT)))
		(if (null AK_BOTTOM_PANEL_DXF_ROTATION) (_FSET (_ 'AK_BOTTOM_PANEL_DXF_ROTATION __MODULBODYMATROT)))
		
; -------------------------------------------------- PANEL DIMENSION DATA ---------------------------------------------------------
		(if (equal BOTTOMSIDE_STYLE  "SIDES_ON_BOTTOM") 
			(progn 
				(_FSET (_ 'bottomPanelWidForBottomSideStyle 0))
			)
			(progn
				(_FSET (_ 'bottomPanelWidForBottomSideStyle __AD_PANELTHICK))
			)
		)
		
		; Global variables of corner unit bottom panel
		(_FSET (_ 'AK_BOTTOM_PANEL_WID (_= "__WID - bottomSideStyleV2 + AK_BOTTOM_PANEL_BACK_VARIANCE")))
		(_FSET (_ 'AK_BOTTOM_PANEL_HEI (_= "__WID2 - bottomSideStyleV2 + AK_BOTTOM_PANEL_BACK_VARIANCE")))
		(_FSET (_ 'AK_BOTTOM_PANEL_DEP (_= "__DEP + AK_BOTTOM_PANEL_BACK_VARIANCE + AK_BOTTOM_PANEL_FRONT_VARIANCE")))
		(_FSET (_ 'AK_BOTTOM_PANEL_DEP2 (_= "__DEP2 + AK_BOTTOM_PANEL_BACK_VARIANCE + AK_BOTTOM_PANEL_FRONT_VARIANCE")))
		
		(_FSET (_ 'AK_BOTTOM_PANEL_ROT (_ AK_BOTTOM_PANEL_MAIN_ROTATION AK_BOTTOM_PANEL_CUTLIST_ROTATION AK_BOTTOM_PANEL_DXF_ROTATION AK_BOTTOM_PANEL_MIRRORING_AXIS)))
		(_FSET (_ 'AK_BOTTOM_PANEL_MAT __MODULBODYMAT))
		
		(_FSET (_ 'AK_BOTTOM_PANEL_LABEL (XSTR PANEL_LABEL)))
		(_FSET (_ 'AK_BOTTOM_PANEL_TAG PANEL_TAG))
		; Conditional codes performed below
		(cond 
			((or (equal __MODULTYPE "AK2") (equal __MODULTYPE "UK2"))
				; 90 degree corner unit
				(if (null AK_BOTTOM_WIDOPPWID_EDGESTRIP_MAT) (_FSET (_ 'AK_BOTTOM_WIDOPPWID_EDGESTRIP_MAT FRONTSIDE_EDGES_MAT)))
				(if (null AK_BOTTOM_WIDOPPHEI_EDGESTRIP_MAT) (_FSET (_ 'AK_BOTTOM_WIDOPPHEI_EDGESTRIP_MAT FRONTSIDE_EDGES_MAT)))
				(_ADDPARTMAT2EDGESTRIPS (_ 'AK_BOTTOM_WID_EDGESTRIP_MAT 'AK_BOTTOM_HEI_EDGESTRIP_MAT 'AK_BOTTOM_DEP_EDGESTRIP_MAT 'AK_BOTTOM_WIDOPPWID_EDGESTRIP_MAT 'AK_BOTTOM_WIDOPPHEI_EDGESTRIP_MAT 'AK_BOTTOM_DEP2_EDGESTRIP_MAT) __MODULBODYMAT ADD_PART_MATERIAL)
				(_FSET (_ 'WIDOPPWID_EDGESTRIP_WIDTH (_GETEDGESTRIPWIDFROMDB AK_BOTTOM_WIDOPPWID_EDGESTRIP_MAT)))
				(_FSET (_ 'WIDOPPHEI_EDGESTRIP_WIDTH (_GETEDGESTRIPWIDFROMDB AK_BOTTOM_WIDOPPHEI_EDGESTRIP_MAT)))
				
				(_FSET (_ 'AK_BOTTOM_PANEL_WIDOPPWID (- __WIDOPPWID bottomSideStyleV2)))
				(_FSET (_ 'AK_BOTTOM_PANEL_WIDOPPHEI (- __WID2OPPWID bottomSideStyleV2)))
				
				(_FSET (_ 'AK_BOTTOM_PANEL_PDATA (_ (_ 0.0 0.0 0.0) 0 
													(_ 0.0 AK_BOTTOM_PANEL_WID 0.0) 0 
													(_ AK_BOTTOM_PANEL_DEP AK_BOTTOM_PANEL_WID 0.0) 0 
													(_ AK_BOTTOM_PANEL_DEP AK_BOTTOM_PANEL_DEP2 0.0) 0 
													(_ AK_BOTTOM_PANEL_HEI AK_BOTTOM_PANEL_DEP2 0.0) 0 
													(_ AK_BOTTOM_PANEL_HEI 0.0 0.0) 0 
													(_ 0.0 0.0 0.0) 0)))
				
				(_FSET (_ 'AK_BOTTOM_PANEL_EDGESTRIPS (_ (_ AK_BOTTOM_WID_EDGESTRIP_MAT WID_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_DEP_EDGESTRIP_MAT DEP_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_WIDOPPWID_EDGESTRIP_MAT WIDOPPWID_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_WIDOPPHEI_EDGESTRIP_MAT WIDOPPHEI_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_DEP2_EDGESTRIP_MAT DEP2_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_HEI_EDGESTRIP_MAT HEI_EDGESTRIP_WIDTH))))
			)
			((or (equal __MODULTYPE "AK3") (equal __MODULTYPE "UK3") (equal __MODULTYPE "AKF"))
				; 135 degree corner unit
				(if (null AK_BOTTOM_SLOPING_EDGESTRIP_MAT) (_FSET (_ 'AK_BOTTOM_SLOPING_EDGESTRIP_MAT FRONTSIDE_EDGES_MAT)))
				(_ADDPARTMAT2EDGESTRIPS (_ 'AK_BOTTOM_WID_EDGESTRIP_MAT 'AK_BOTTOM_HEI_EDGESTRIP_MAT 'AK_BOTTOM_DEP_EDGESTRIP_MAT 'AK_BOTTOM_DEP2_EDGESTRIP_MAT 'AK_BOTTOM_SLOPING_EDGESTRIP_MAT) __MODULBODYMAT ADD_PART_MATERIAL)
				(_FSET (_ 'SLOPING_EDGESTRIP_WIDTH (_GETEDGESTRIPWIDFROMDB AK_BOTTOM_SLOPING_EDGESTRIP_MAT)))
				
				(_FSET (_ 'AK_BOTTOM_PANEL_SLOPING_EDGE (sqrt (+ (expt (- AK_BOTTOM_PANEL_HEI AK_BOTTOM_PANEL_DEP) 2) (expt (- AK_BOTTOM_PANEL_WID AK_BOTTOM_PANEL_DEP2) 2)))))
				
				(_FSET (_ 'TanOfAngleForBottomPanel (/ (- (+ AK_BOTTOM_PANEL_WID bottomPanelWidForBottomSideStyle) AK_BOTTOM_PANEL_DEP2) (- (+ AK_BOTTOM_PANEL_HEI bottomPanelWidForBottomSideStyle) AK_BOTTOM_PANEL_DEP))))

				(_FSET (_ 'AK_BOTTOM_PANEL_PDATA (_ (_ 0.0 0.0 0.0) 0 
													(_ 0.0 AK_BOTTOM_PANEL_WID 0.0) 0 
													;(_ AK_BOTTOM_PANEL_DEP AK_BOTTOM_PANEL_WID 0.0) 0 
													(_ (+ AK_BOTTOM_PANEL_DEP (/ bottomPanelWidForBottomSideStyle TanOfAngleForBottomPanel))AK_BOTTOM_PANEL_WID 0.0) 0
													;(_ AK_BOTTOM_PANEL_HEI AK_BOTTOM_PANEL_DEP2 0.0) 0 
													(_ AK_BOTTOM_PANEL_HEI (+ AK_BOTTOM_PANEL_DEP2 (* bottomPanelWidForBottomSideStyle TanOfAngleForBottomPanel)) 0.0) 0
													(_ AK_BOTTOM_PANEL_HEI 0.0 0.0) 0 
													(_ 0.0 0.0 0.0) 0)))
				
				(_FSET (_ 'AK_BOTTOM_PANEL_EDGESTRIPS (_ (_ AK_BOTTOM_WID_EDGESTRIP_MAT WID_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_DEP_EDGESTRIP_MAT DEP_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_SLOPING_EDGESTRIP_MAT SLOPING_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_DEP2_EDGESTRIP_MAT DEP2_EDGESTRIP_WIDTH)
														 (_ AK_BOTTOM_HEI_EDGESTRIP_MAT HEI_EDGESTRIP_WIDTH))))
			)
		)
		(_FSET (_ 'AK_BOTTOM_PANEL_CODE (_& (_ (XSTR "CORNER_UNIT") "_" (XSTR "BOTTOM_PANEL")))))

; -------------------------------------------------- CREATING PANEL ---------------------------------------------------------
		(_PANELMAIN AK_BOTTOM_PANEL_CODE (_ AK_BOTTOM_PANEL_PDATA AK_BOTTOM_PANEL_ROT AK_BOTTOM_PANEL_MAT AK_BOTTOM_PANEL_THICKNESS))
		
		(_CREATEEDGESTRIPS AK_BOTTOM_PANEL_CODE AK_BOTTOM_PANEL_EDGESTRIPS)
		
		(_PUTLABEL AK_BOTTOM_PANEL_CODE AK_BOTTOM_PANEL_CODE AK_BOTTOM_PANEL_LABEL)
		(_PUTTAG AK_BOTTOM_PANEL_CODE AK_BOTTOM_PANEL_CODE AK_BOTTOM_PANEL_TAG)
		; IMPORTANT INFORMATION -> UPPER face is the primary operation face
		; OPERATION FACE SELECTION
		(if (_NOTNULL BOTTOM_PANEL_OP_ON_LOWER_FACE)
			(_FSET (_ 'AK_BOTTOM_PANEL_OP_FACE (_CREATESFACEMAIN AK_BOTTOM_PANEL_CODE (_ AK_BOTTOM_PANEL_PDATA AK_BOTTOM_PANEL_ROT AK_BOTTOM_PANEL_MAT AK_BOTTOM_PANEL_THICKNESS "X"))))
			(_FSET (_ 'AK_BOTTOM_PANEL_OP_FACE AK_BOTTOM_PANEL_CODE))
		)
	)
)